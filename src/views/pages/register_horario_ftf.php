<?php 
  $profesiones = profesionController::getAllProfesionRegister();
   
?>
<div class="login-page">  
  <?php
        if(empty($profesiones)){
          echo "<p class='login-box-msg' style='color:red; font-size:20px;'>Profesiones vacias, Debes registrar alguna!</p>";
        }
  ?>
  <div class="register-box">
    
    <h3 align="center"><b>Registro</b> </br>Nuevo Horario Face to Face</h3>
    
    <div class="card">
      <div class="card-body register-card-body">
        <p class="login-box-msg">Rellenar todos los datos</p>
   
        <form method="post">
          <div class="input-group mb-3">
          <div class="input-group mb-3">
            <input class="form-control" type="time" id="hora_inicio">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <label >hora de finalizar</label>
          <div class="input-group mb-3">
            <input class="form-control" type="time" id="hora_finalizar">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="" id="diasemana" placeholder="Dias de la semana (ejemplo, ejemplo, etc)">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div> 
            </div>
          </div>
            <input type="text" class="form-control" name="" id="linkZoom" placeholder="Link de Clase">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div> 
            </div>
          </div>
          <div class="input-group mb-3">
            <input class="form-control" type="number" id="nro_client" placeholder="Nro de clientes">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <select class="form-control" name="rolAdmin" id="profesion_id" >
              <option value="">Profesion</opton>
              <?php foreach($profesiones as $profesion): ?>
                <option value="<?php echo $profesion['id'] ?>"><?php echo $profesion['nombre'] ?></opton>
              <?php endforeach; ?>
            </select> 
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
     
          <div class="input-group mb-3">
            <select class="form-control" name="rolAdmin" id="curso_id" placeholder="Role"  disabled>
              <option value="">curso</opton>
              
            </select> 
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <select class="form-control" name="rolAdmin" id="profesor_id" placeholder="Role" disabled>
              <option value="">Profesor</opton>
              
            </select> 
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          
          <div class="row">
       
            <!-- /.col -->
            <div class="col-12">
              <button type="submit" class="btn btn-primary btn-block" id="registerHorario_ftf">Register</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>
      <!-- /.form-box -->
    </div><!-- /.card -->
  </div>
<!-- /.register-box -->
</div>

