
<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo $urls; ?>" class="brand-link">
      <img src="<?php echo $urls; ?>src/public/img/logo.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Torum Panel Admin</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="#" class="d-block"><i class="fas fa-user-tie nav-icon"></i> <span> <?php echo $_SESSION['name']; ?></span></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
          </li>
          <li class="nav-item">
            <a href="<?php echo $urls; ?>clientes" class="nav-link">
              <i class="nav-icon far fa-address-book"></i>
              <p>
                Clientes
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo $urls; ?>clientes_cursando_plus" class="nav-link">
            <i class="nav-icon far fa-clock"></i>
              <p>
                Cursando Plus Clientes
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo $urls; ?>lista_pagos" class="nav-link">
              <i class="nav-icon far fa-address-book"></i>
              <p>
                Lista de Pagos
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-clock"></i>
              <p>
                Horarios Grupales
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo $urls; ?>horarios" class="nav-link">
                  <i class="fas fa-clipboard-list nav-icon"></i>
                  <p>Lista</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo $urls; ?>register_horario" class="nav-link">
                  <i class="fas fa-plus nav-icon"></i>
                  <p>crear</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-clock"></i>
              <p>
                Horarios Face to Face
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo $urls; ?>horarios_ftf" class="nav-link">
                  <i class="fas fa-clipboard-list nav-icon"></i>
                  <p>Lista</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo $urls; ?>register_horario_ftf" class="nav-link">
                  <i class="fas fa-plus nav-icon"></i>
                  <p>crear</p>
                </a>
              </li>
            </ul>
          </li>
          <?php if($_SESSION["rol"] == "SUPER_ADMIN"): ?>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chalkboard-teacher"></i>
              <p>
                Cursos
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo $urls; ?>cursos" class="nav-link">
                  <i class="fas fa-clipboard-list nav-icon"></i>
                  <p>Lista</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo $urls; ?>register_curso" class="nav-link">
                  <i class="fas fa-plus nav-icon"></i>
                  <p>crear</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fas fa-users"></i>
              <p>
                Cursos Instructores
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo $urls; ?>cursos_grupal" class="nav-link">
                  <i class="fas fa-clipboard-list nav-icon"></i>
                  <p>Lista</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo $urls; ?>register_curso_grupal" class="nav-link">
                  <i class="fas fa-plus nav-icon"></i>
                  <p>crear</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-graduation-cap"></i>
              <p>
                Profesiones
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo $urls; ?>profesiones" class="nav-link">
                  <i class="fas fa-clipboard-list nav-icon"></i>
                  <p>Lista</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo $urls; ?>crear-profesion" class="nav-link">
                  <i class="fas fa-plus nav-icon"></i>
                  <p>crear</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
             <i class="nav-icon far fa-address-book"></i>
              <p>
                Administradores
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="<?php echo $urls; ?>admins" class="nav-link">
                <i class="nav-icon far fa-address-book"></i>
                  <p>Lista</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo $urls; ?>register_admin" class="nav-link">
                  <i class="fas fa-user-plus nav-icon"></i>
                  <p>Registrar</p>
                </a>
              </li>
            </ul>
          </li>
          <?php endif; ?>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fas fa-user-graduate"></i>
              <p>
                Instructores
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
              <a href="<?php echo $urls; ?>profesores" class="nav-link">
                <i class="nav-icon far fa-address-book"></i>
                <p>
                  Lista
                </p>
              </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo $urls; ?>register_teacher" class="nav-link">
                  <i class="fas fa-user-plus nav-icon"></i>
                  <p>Registrar</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="<?php echo $urls; ?>close" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Cerrar Sesion
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>