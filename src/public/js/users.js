$( document ).ready(function() { 

    $(".statusCliente").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();

       var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/users_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    var st = $("#clienteStatus_"+id).attr("class");
                    if(st == "badge badge-danger"){
                      
                        $("#clienteStatus_"+id).attr("class","badge badge-success");
                        $("#clienteStatus_"+id).text("Activo");
                        
                     
                        $("#statusCliente_"+id).addClass("btn-danger");
                        $("#statusCliente_"+id).removeClass("btn-success");
                        $("#statusCliente_"+id).text("Inhabilitar ");
                 

                    }else if(st == "badge badge-success"){

                        $("#clienteStatus_"+id).attr("class","badge badge-danger");
                        $("#clienteStatus_"+id).text("Inactivo");
                 

                        $("#statusCliente_"+id).addClass("btn-success");
                        $("#statusCliente_"+id).removeClass("btn-danger");
                        $("#statusCliente_"+id).text("Habilitar");
                        

                    }
                    console.log(st);
                }else if(response == "error"){
                    console.log("erro");
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("statusUsuario=true&status="+ id);

    })

    $(".getCliente").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();

        var ajax = new XMLHttpRequest();
        var URL = url +'src/ajax/users_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response !== "") {
                    var obj = jQuery.parseJSON(response);
                    $("#idClient").val(obj.id);
                    $("#nameCLient").val(obj.nombre);
                    $("#lastNameCLient").val(obj.apellido);
                    $("#phoneCLient").val(obj.telefono);
                    $("#emailClient").val(obj.mail);
                    //$("#passClient").val(obj.email);                      
                   
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("getUserId=true&id="+ id);
    })

    $("#editCliente").on('click',  function(event) {
        event.preventDefault();
        var id = $("#idClient").val();
        var nombre = $("#nameCLient").val();
        var apellido = $("#lastNameCLient").val();
        var telefono = $("#phoneCLient").val();
        var mail = $("#emailClient").val();
        //var id = $("#passClient").val();
        var url = $("#url").val();

        if (nombre =='') {
            swal('Error', 'Error nombre del cliente vacio!', 'error');
        
            return false;
        };

        
        if (apellido =='') {
            swal('Error', 'Error apellido del cliente!', 'error');
        
            return false;
        };
        if (telefono =='') {
            swal('Error', 'Error telefono del cliente!', 'error');
        
            return false;
        };
        if (mail =='') {
            swal('Error', 'Error correo del cliente!', 'error');
        
            return false;
        }else {
            var mail_user = EsEmail(mail);
            if(mail_user == false){
                swal('Error', 'Error correo no correcto!', 'error');
                return false;
            }
        };

        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/users_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    swal({
                        title: "¡OK!",
                        text: "Horario Editado exitosamente!",
                        type: "success",
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    },
                    
                    function(isConfirm) {
                        if (isConfirm) {
                            window.location.reload();
                    
                        }
                    });
                }else{

					swal('Error', 'Error no se ha podido actualizar el cliente!', 'error');

				};;
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("editUsuario=true&idClient="+id+"&nombre="+ nombre + "&apellido="+ apellido+ "&telefono="+ telefono + "&mail="+ mail);
   
    })

    function EsEmail(w_email) {
    
        var test = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var emailReg = new RegExp(test);
    
        return responseTestEmail = emailReg.test(w_email);
    }
})