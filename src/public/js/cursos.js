$( document ).ready(function() {

    
    $("#registerCurso").on('click',  function(event) {
        event.preventDefault();
        for (instance in CKEDITOR.instances) 
        {
            CKEDITOR.instances[instance].updateElement();
        }
        
        var nameCurso = $("#nameCurso").val();
        var videosCurso = $("#videosCurso").val();
        var plus = $("#plusCurso").val();
        var form = $("#form_curso").serialize();
        var profesionCurso = $("#profesionCurso").val();
        var url = $("#url").val();
     
        if (nameCurso =='') {
            swal('Error', 'Error nombre vacio!', 'error');
        
            return false;
        };
     
        if (videosCurso =='') {
            swal('Error', 'Error cantidad de videos vacio!', 'error');
        
            return false;
        };
        if (plus =='') {
            swal('Error', 'Error cantidad de videos vacio!', 'error');
        
            return false;
        };
        if (profesionCurso =='') {
            swal('Error', 'Error Elija una profesion!', 'error');
        
            return false;
        };

        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/curso_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    swal({
                        title: "¡OK!",
                        text: "Curso Registrado exitosamente!",
                        type: "success",
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    },
                    
                    function(isConfirm) {
                        if (isConfirm) {
                            window.location.reload();
                    
                        }
                    });
                }else if (response == "exist") {

					swal('Error', 'Error curso ya existe!', 'error');

				};
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("registerCurso=true&form="+ form);
    

    })

    $(".statusCurso").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();

       var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/curso_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    var st = $("#cursoStatus_"+id).attr("class");
                    if(st == "badge badge-danger"){
                      
                        $("#cursoStatus_"+id).attr("class","badge badge-success");
                        $("#cursoStatus_"+id).text("Activo");
                        
                     
                        $("#statusCurso_"+id).addClass("btn-danger");
                        $("#statusCurso_"+id).removeClass("btn-success");
                        $("#statusCurso_"+id).text("Inhabilitar ");
                 

                    }else if(st == "badge badge-success"){

                        $("#cursoStatus_"+id).attr("class","badge badge-danger");
                        $("#cursoStatus_"+id).text("Inactivo");
                 

                        $("#statusCurso_"+id).addClass("btn-success");
                        $("#statusCurso_"+id).removeClass("btn-danger");
                        $("#statusCurso_"+id).text("Habilitar");
                        

                    }
                    console.log(st);
                }else if(response == "error"){
                    console.log("erro");
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("statusCurso=true&status="+ id);

    })

    $(".getCurso").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();
        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/curso_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response !== "") {
                    var obj = jQuery.parseJSON(response);
                    $("#idCurso").val(obj.id);
                    $("#nameCurso").val(obj.nombre);
                    $("#videosCurso").val(obj.numero_videos);
                    $("#plus_curso").val(obj.plus);
                    $("#profesionCurso").val(obj.profesion_id);
                    $(".cke_editable").val(obj.descripcion);
                    console.log(obj.descripcion);
                    
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("getCursoId=true&id="+ id);
    })

    $("#editCurso").on('click',  function(event) {
        event.preventDefault();

        for (instance in CKEDITOR.instances) 
        {
            CKEDITOR.instances[instance].updateElement();
        }
        var idCurso = $("#idCurso").val();
        var nameCurso = $("#nameCurso").val();
        var videosCurso = $("#videosCurso").val();
        var plus = $("#plus_curso").val();
        var profesionCurso = $("#profesionCurso").val();
        var url = $("#url").val();
        var form = $("#form_curso").serialize();
        if (nameCurso =='') {
            swal('Error', 'Error nombre vacio!', 'error');
        
            return false;
        };
     
        if (videosCurso =='') {
            swal('Error', 'Error cantidad de videos vacio!', 'error');
        
            return false;
        };
        if (plus =='') {
            swal('Error', 'Error cantidad de videos vacio!', 'error');
        
            return false;
        };
        if (profesionCurso =='') {
            swal('Error', 'Error Elija una profesion!', 'error');
        
            return false;
        };

        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/curso_ajax.php';
        var method = "POST"; 

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    swal({
                        title: "¡OK!",
                        text: "¡Administrador Editado exitosamente!",
                        type: "success",
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    },
                    
                    function(isConfirm) {
                        if (isConfirm) {
                            window.location.reload();
                    
                        }
                    });
                }else if (response == "exist") {

					swal('Error', 'Error ya este usuario o correo ya existe!', 'error');

				};
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("editCurso=true&form="+form);
    

    })


    $(".statusCursandoPlus").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();

       var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/curso_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    var st = $("#cursandoStatusPlus_"+id).attr("class");
                    if(st == "badge badge-danger"){
                      
                        $("#cursandoStatusPlus_"+id).attr("class","badge badge-success");
                        $("#cursandoStatusPlus_"+id).text("Activo");
                        
                     
                        $("#statusCursandoPlus_"+id).addClass("btn-danger");
                        $("#statusCursandoPlus_"+id).removeClass("btn-success");
                        $("#statusCursandoPlus_"+id).text("Inhabilitar ");
                 

                    }else if(st == "badge badge-success"){

                        $("#cursandoStatusPlus_"+id).attr("class","badge badge-danger");
                        $("#cursandoStatusPlus_"+id).text("Inactivo");
                 

                        $("#statusCursandoPlus_"+id).addClass("btn-success");
                        $("#statusCursandoPlus_"+id).removeClass("btn-danger");
                        $("#statusCursandoPlus_"+id).text("Habilitar");
                        

                    }
                    console.log(st);
                }else if(response == "error"){
                    console.log("erro");
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("statusCursandoPlus=true&status="+ id);

    })


    $("#registerCursandoPlus").on('click',  function(event) {
        event.preventDefault();
        
        var mailCliente = $("#emailClient").val();
        var planCurso = $("#planPlusCursando").val();
        var curso = $("#cursoCursando").val();
        
        var url = $("#url").val();
     
        if (mailCliente =='') {
            swal('Error', 'Error correo vacio!', 'error');
        
            return false;
        }else {
            var mail_user = EsEmail(mailCliente);
            if(mail_user == false){
                swal('Error', 'Error correo no correcto!', 'error');
                return false;
            };
        }
     
        if (planCurso =='') {
            swal('Error', 'Error plan vacio!', 'error');
        
            return false;
        };
        if (curso =='') {
            swal('Error', 'Error curso de videos vacio!', 'error');
        
            return false;
        };
        

        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/curso_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    swal({
                        title: "¡OK!",
                        text: "Curso Registrado exitosamente!",
                        type: "success",
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    },
                    
                    function(isConfirm) {
                        if (isConfirm) {
                            window.location.reload();
                    
                        }
                    });
                }else if (response == "no_exist_user") {

					swal('Error', 'Error usuario no existe!', 'error');

				}else if (response == "error") {

					swal('Error', 'Error no pudo asginar curso a usuario!', 'error');

				};
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("registerCursandoPlus=true&correo_client="+ mailCliente+ "&planCurso="+planCurso+"&curso="+curso);
    

    })

    function EsEmail(w_email) {
    
        var test = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var emailReg = new RegExp(test);
    
        return responseTestEmail = emailReg.test(w_email);
    }
        
});
