<?php 
  $profesiones = profesionController::getAllProfesionRegister();
?>
   
<style>
@media (max-width: 600px) {
  .form_editor{
    margin-left:5% !important;
  }
}
</style>
<div class="form_editor" style="margin-left:40%;">  
  <?php
        if(empty($profesiones)){
          echo "<p class='login-box-msg' style='color:red; font-size:20px;'>Profesiones vacias, Debes registrar alguna!</p>";
        }
  ?>
  <div class="register-box">
    
    <h2 align="center"><b>Registro</b> </br>Nuevo Curso</h2>
    
    <div class="card">
      <div class="card-body register-card-body">
        <p class="login-box-msg">Rellenar todos los datos</p>
   
        <form method="post" id="form_curso">
          <div class="input-group mb-3">
          <input type="hidden" name="nameCurso2" id="nameCurso2">
            <input type="text" class="form-control" name="nameCurso" id="nameCurso" placeholder="Nombre Del Curso">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="number" class="form-control" name="videosCurso" id="videosCurso" placeholder="Catidad de Videos (Exactos)">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <select class="form-control" name="plusCursos" id="plusCurso" placeholder="Role">
              <option value="">Selecione si es o no Plus</opton>
              <option value="1">Si</opton>
              <option value="0">no</opton>              
            </select> 
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <select class="form-control" name="profesionCurso" id="profesionCurso" placeholder="Role">
              <option value="">Profesion</opton>
              <?php foreach($profesiones as $profesion): ?>
                <option value="<?php echo $profesion['id'] ?>"><?php echo $profesion['nombre'] ?></opton>
              <?php endforeach; ?>
            </select> 
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div> 
          <div class="input-group mb-3">
          <label>Descripcion del curso</label>
          <textarea class="editor1" name="editor1" id="editor"></textarea>
          </div>
          
          <div class="row">
       
            <!-- /.col -->
            <div class="col-12">
              <button type="submit" class="btn btn-primary btn-block" name="registerCurso" id="registerCurso">Register</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>
      <!-- /.form-box -->
    </div><!-- /.card -->
  </div>
<!-- /.register-box -->
</div>

