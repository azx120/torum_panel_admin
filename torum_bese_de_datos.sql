-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-11-2020 a las 15:48:25
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `torum`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `plus` varchar(200) NOT NULL,
  `numero_videos` int(11) NOT NULL,
  `profesion_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id`, `nombre`, `plus`, `numero_videos`, `profesion_id`, `status`) VALUES
(1, 'curso 1', '0', 15, 1, 1),
(2, 'curso 2', '1', 12, 2, 0),
(6, 'curso 6', '0', 12, 3, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos_grupal`
--

CREATE TABLE `cursos_grupal` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `profesion_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cursos_grupal`
--

INSERT INTO `cursos_grupal` (`id`, `nombre`, `profesion_id`, `status`) VALUES
(1, 'cuso g 11', 2, 1),
(2, 'curso 3', 1, 1),
(3, 'curso  4 ', 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarios`
--

CREATE TABLE `horarios` (
  `id` int(11) NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_finalizar` time NOT NULL,
  `link_zoom` varchar(300) NOT NULL,
  `profesor_id` int(11) NOT NULL,
  `curso_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `horarios`
--

INSERT INTO `horarios` (`id`, `hora_inicio`, `hora_finalizar`, `link_zoom`, `profesor_id`, `curso_id`, `status`) VALUES
(1, '01:00:00', '02:00:00', 'https://www.google.com/', 3, 1, 1),
(2, '01:00:00', '02:00:00', 'https://www.youtube.com/', 3, 1, 1),
(3, '01:00:00', '02:00:00', 'https://www.youtube.com/jdfhdjf', 3, 1, 1),
(4, '01:00:00', '02:00:00', 'https://www.youtube.com/', 2, 1, 1),
(5, '01:00:00', '01:00:00', 'ddfdsf', 3, 1, 1),
(6, '01:00:00', '14:01:00', 'cdds', 4, 1, 1),
(7, '01:00:00', '01:00:00', 'vcvcxv', 2, 1, 1),
(8, '02:00:00', '01:00:00', 'jjh,mj', 3, 1, 1),
(9, '01:00:00', '01:00:00', 'rry', 2, 1, 1),
(10, '01:00:00', '01:00:00', 'fg', 4, 1, 1),
(11, '13:00:00', '01:00:00', 'hgfh', 4, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesiones`
--

CREATE TABLE `profesiones` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `rama` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `profesiones`
--

INSERT INTO `profesiones` (`id`, `nombre`, `rama`, `status`) VALUES
(1, 'NUTRICIÓN ', 'MEDICINA ', 1),
(2, 'ENTRENAMIENTO', 'DEPORTE', 1),
(3, 'CARPINTERÍA', 'CIVIL', 1),
(4, 'OTRA', 'OTRA', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesores`
--

CREATE TABLE `profesores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `apellido` varchar(200) NOT NULL,
  `mail` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `profesion_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `profesores`
--

INSERT INTO `profesores` (`id`, `nombre`, `apellido`, `mail`, `password`, `profesion_id`, `status`) VALUES
(1, 'Alfredo Javier', 'zerpa', 'xoscuro3@gmail.com', 'alfredo120', 1, 1),
(2, 'Alfredo Javier ', 'zerpa', 'xoscuro@gmail.com', 'alfredo120', 2, 1),
(3, 'Stefy', 'Fuentes', 'stefy@gmail.com', 'carasucia030', 2, 1),
(4, 'Alfonso', 'Rivas', 'xoscuro3@gmail.com', 'alfredo120', 2, 1),
(5, 'Alfonso', 'Rivas', 'xoscuro3@gmail.com', 'alfredo120', 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_admin`
--

CREATE TABLE `user_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(150) NOT NULL,
  `name` varchar(200) NOT NULL,
  `mail` varchar(150) NOT NULL,
  `password` varchar(300) NOT NULL,
  `rol` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user_admin`
--

INSERT INTO `user_admin` (`id`, `username`, `name`, `mail`, `password`, `rol`, `status`) VALUES
(1, 'xoscuro3', 'Alfredo Zerpa', 'xoscuro3@gmail.com', '$2a$07$asxx54ahjppf45sd87a5au.WKxJBAvhGBtSxmD1maQIfYRg4lc0Di', 'SUPER_ADMIN', 1),
(2, 'azx', 'Javier Perez', 'azx@gmail.com', '$2a$07$asxx54ahjppf45sd87a5auL/V6u08ILLGjvGkLvEiJuNlFan/tbdu', 'ADMIN', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `fecha_subida` date NOT NULL,
  `profesion_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos_curso`
--

CREATE TABLE `videos_curso` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `nro_video` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `curso_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `videos_curso`
--

INSERT INTO `videos_curso` (`id`, `titulo`, `nro_video`, `status`, `curso_id`, `video_id`) VALUES
(1, 'video 1', 1, 0, 1, 1),
(2, 'video 2', 2, 0, 1, 2),
(3, 'video 5', 5, 0, 1, 2),
(4, 'video 4', 4, 0, 1, 1),
(5, 'video 3', 3, 0, 1, 3),
(6, 'video 6', 6, 0, 1, 2),
(7, 'video 7', 7, 0, 1, 1),
(8, 'video 8', 8, 0, 1, 3);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cursos_grupal`
--
ALTER TABLE `cursos_grupal`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `horarios`
--
ALTER TABLE `horarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `profesiones`
--
ALTER TABLE `profesiones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `profesores`
--
ALTER TABLE `profesores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_admin`
--
ALTER TABLE `user_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `videos_curso`
--
ALTER TABLE `videos_curso`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `cursos_grupal`
--
ALTER TABLE `cursos_grupal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `horarios`
--
ALTER TABLE `horarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `profesiones`
--
ALTER TABLE `profesiones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `profesores`
--
ALTER TABLE `profesores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `user_admin`
--
ALTER TABLE `user_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `videos_curso`
--
ALTER TABLE `videos_curso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
