$( document ).ready(function() {

     
    $("#registerCursoGrupal").on('click',  function(event) {
        event.preventDefault();
    
        var nameCurso = $("#nameCurso").val();
        var profesionCurso = $("#profesionCurso").val();
        var botonFtf = $("#botonFtf").val();
        var url = $("#url").val();

        if (nameCurso =='') {
            swal('Error', 'Error nombre vacio!', 'error');
        
            return false;
        };
     
        if (profesionCurso =='') {
            swal('Error', 'Error Elija una profesion!', 'error');
        
            return false;
        };
        if (botonFtf =='') {
            swal('Error', 'Ingrese un enlace de boton de pago!', 'error');
        
            return false;
        };
    

        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/curso_grupal_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    swal({
                        title: "¡OK!",
                        text: "Profesor curso exitosamente!",
                        type: "success",
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    },
                    
                    function(isConfirm) {
                        if (isConfirm) {
                            window.location.reload();
                    
                        }
                    });
                }else if (response == "exist") {

					swal('Error', 'Error ya este curso esta registrado!', 'error');

				};
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("registerCursoGrupal=true&nameCurso="+ nameCurso + "&profesionCurso="+ profesionCurso+"&botonFtf="+ botonFtf);
    

    })

    $(".statusCursoGrupal").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();

       var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/curso_grupal_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    var st = $("#cursoStatus_"+id).attr("class");
                    if(st == "badge badge-danger"){
                      
                        $("#cursoStatus_"+id).attr("class","badge badge-success");
                        $("#cursoStatus_"+id).text("Activo");
                        
                     
                        $("#statusCurso_"+id).addClass("btn-danger");
                        $("#statusCurso_"+id).removeClass("btn-success");
                        $("#statusCurso_"+id).text("Inhabilitar ");
                 

                    }else if(st == "badge badge-success"){

                        $("#cursoStatus_"+id).attr("class","badge badge-danger");
                        $("#cursoStatus_"+id).text("Inactivo");
                 

                        $("#statusCurso_"+id).addClass("btn-success");
                        $("#statusCurso_"+id).removeClass("btn-danger");
                        $("#statusCurso_"+id).text("Habilitar");
                        

                    }
                }else if(response == "error"){
                    console.log("erro");
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("statusCursoGrupal=true&status="+ id);

    })

    $(".getCursoGrupal").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();
        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/curso_grupal_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response !== "") {
                    var obj = jQuery.parseJSON(response);
                    $("#idCurso").val(obj.id);
                    $("#nameCurso").val(obj.nombre);
                    $("#botonFtf").val(obj.boton_ftf);
                    $("#profesionCurso").val(obj.profesion_id);
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("getCursoGrupalId=true&id="+ id);
    })

    $("#editCursoGrupal").on('click',  function(event) {
        event.preventDefault();
        var idCurso = $("#idCurso").val();
        var nameCurso = $("#nameCurso").val();
        var profesionCurso = $("#profesionCurso").val();
        var botonFtf = $("#botonFtf").val();
        var url = $("#url").val();

        if (nameCurso =='') {
            swal('Error', 'Error nombre vacio!', 'error');
        
            return false;
        };
     
        if (profesionCurso =='') {
            swal('Error', 'Error Elija una profesion!', 'error');
        
            return false;
        };
        if (botonFtf =='') {
            swal('Error', 'Ingrese un enlace de boton de pago!', 'error');
        
            return false;
        };

        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/curso_grupal_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    swal({
                        title: "¡OK!",
                        text: "¡Administrador Editado exitosamente!",
                        type: "success",
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    },
                    
                    function(isConfirm) {
                        if (isConfirm) {
                            window.location.reload();
                    
                        }
                    });
                }else if (response == "exist") {

					swal('Error', 'Error ya este usuario o correo ya existe!', 'error');

				};
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("editCursoGrupal=true&idCurso="+idCurso+"&nameCurso="+ nameCurso +"&profesionCurso="+ profesionCurso+"&botonFtf="+ botonFtf);
    

    })

    $("#registerClientCursoGrupal").on('click',  function(event) {
        event.preventDefault();
    
        var id_horario = $("#id_horario").val();
        var mailCliente = $("#mailCliente").val();
        var url = $("#url").val();

        if (mailCliente =='') {
            swal('Error', 'Error correo vacio!', 'error');
            return false;
        }else {
            var mail = EsEmail(mailCliente);
            if(mail == false){
                swal('Error', 'Error correo no correcto!', 'error');
                return false;
            }
        };
    
        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/curso_grupal_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    swal({
                        title: "¡OK!",
                        text: "Cliente registrado a curso exitosamente!",
                        type: "success",
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    },
                    
                    function(isConfirm) {
                        if (isConfirm) {
                            window.location.reload();
                    
                        }
                    });
                }else if (response == "exist") {

					swal('Error', 'Error Cliente ya esta registrado a este curso!', 'error');

				}else if (response == "no_exist_user") {

					swal('Error', 'Error Cliente no existe!', 'error');

				};
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("addClientCursoGrupal=true&id_horario="+ id_horario + "&correo_client="+ mailCliente);
    

    })



    $(".statusCursoGrupalCliente").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();

       var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/curso_grupal_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    var st = $("#cursandoGrupalStatus_"+id).attr("class");
                    if(st == "badge badge-danger"){
                      
                        $("#cursandoGrupalStatus_"+id).attr("class","badge badge-success");
                        $("#cursandoGrupalStatus_"+id).text("Activo");
                        
                     
                        $("#statusCursandoGrupal_"+id).addClass("btn-danger");
                        $("#statusCursandoGrupal_"+id).removeClass("btn-success");
                        $("#statusCursandoGrupal_"+id).text("Inhabilitar ");
                 

                    }else if(st == "badge badge-success"){

                        $("#cursandoGrupalStatus_"+id).attr("class","badge badge-danger");
                        $("#cursandoGrupalStatus_"+id).text("Inactivo");
                 

                        $("#statusCursandoGrupal_"+id).addClass("btn-success");
                        $("#statusCursandoGrupal_"+id).removeClass("btn-danger");
                        $("#statusCursandoGrupal_"+id).text("Habilitar");
                        

                    }
                }else if(response == "error"){
                    console.log("erro");
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("statusCursoGrupalCliente=true&status="+ id);

    })


    function EsEmail(w_email) {
    
        var test = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var emailReg = new RegExp(test);
    
        return responseTestEmail = emailReg.test(w_email);
    }
        
});
