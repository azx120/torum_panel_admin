<?php

class profesorController{

    public static function allProfesor(){

        $prefesiones = new profesorModel();
        $all_prefesiones = $prefesiones -> getAllProfesor();
        return $all_prefesiones;
    }

    public function getProfesorById($post){

        $profesores = new profesorModel();
        $profesor_id = $profesores -> getProfesorId($post);
        return $profesor_id;
    }

    public function editProfesor($post){

        $profesor = new profesorModel();

        $getProfesor = $profesor -> getProfesor($post);

        if ($getProfesor['id'] == $post['idProfesor'] || empty($getProfesor)){
            $nombre_archivo = $_FILES['imgPerson']['name'];
            $img_up = upload::upServer($_POST);

           $profesor_id = $profesor -> ProfesorEdit($post);

            if ($profesor_id == "ok"){
                return "ok";
            }else {
                return "error";
            }
        }else{
            return "exist";
        } 
    }

    public function statusProfesor($post){

        $profesores = new profesorModel();
        $all_profesores = $profesores -> statusProfesor($post);
        return $all_profesores;
    }
     
    public function createProfesor($post) {

        $nombre_archivo = $_FILES['imgPerson']['name'];
        $img_up = upload::upServer($_POST);

        $profesorNew = new profesorModel();
        $profesor = $profesorNew -> getProfesor($post);
        if ($profesor == "" && $profesor == null && $profesor == false){
            $register = $profesorNew -> newProfesor($post);
            if ($register == "ok"){
                return "ok";
            }else {
                return "error";
            }
        }else{
            return "exist";
        }    
    }
 
}



?>
