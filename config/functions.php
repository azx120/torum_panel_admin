<?php 


//Función url amigable
function limpiar($url) {
    // Tranformamos todo a minusculas
    $url = strtolower($url);
    //Rememplazamos caracteres especiales latinos
    $find = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
    $repl = array('a', 'e', 'i', 'o', 'u', 'n');
    $url = str_replace ($find, $repl, $url);
    // Añadimos los guiones
    $find = array(' ', '&', '\r\n', '\n', '+'); 
    $url = str_replace ($find, '-', $url);
    // Eliminamos y Reemplazamos demás caracteres especiales
    $find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
    $repl = array('', '-', '');
    $url = preg_replace ($find, $repl, $url);
    
    return $url;
}


//Función url amigable
function replace_sap($url) {
    // Tranformamos todo a minusculas
    $url = strtolower($url);
    //Rememplazamos caracteres especiales latinos
    $find = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
    $repl = array('a', 'e', 'i', 'o', 'u', 'n');
    $url = str_replace ($find, $repl, $url);
    // Añadimos los guiones
    $find = array(' ', '&', '\r\n', '\n', '+'); 
    $url = str_replace ($find, '-', $url);
    // Eliminamos y Reemplazamos demás caracteres especiales
    $find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
    $repl = array('', '_', '');
    $url = preg_replace ($find, $repl, $url);
    
    return $url;
}



/*--=============================================
FUNCIONES PARA TIEDA
==============================================--*/

function logo_store($logo_store){

  $stmt = Conexion::conn()->prepare("SELECT * FROM tc_store WHERE store = :store");

      $stmt -> bindParam(":store", $logo_store, PDO::PARAM_STR);

      $stmt -> execute();

      if ($f = $stmt->fetch(PDO::FETCH_ASSOC)) {
        return $f['logo'];
      }
 
    $stmt = null;
  
}

/*--=============================================
=            Function view coutry            =
==============================================--*/

function country($idcountry){

  $stmt = Conexion::conn()->prepare("SELECT * FROM tc_country WHERE id = :id");

      $stmt -> bindParam(":id", $idcountry, PDO::PARAM_INT);

      $stmt -> execute();

      if ($f = $stmt->fetch(PDO::FETCH_ASSOC)) {
        return $f['country'];
      }
 
    $stmt = null;
  
}

function provi($idciudad){

  $stmt = Conexion::conn()->prepare("SELECT * FROM tc_provi WHERE idprovi = :id");

      $stmt -> bindParam(":id", $idciudad, PDO::PARAM_INT);

      $stmt -> execute();

      if ($f = $stmt->fetch(PDO::FETCH_ASSOC)) {
        return $f['country'];
      }
 
    $stmt = null;
  
}


/*--=============================================
FUNCIONES PARA LOS PRODUCTOS
==============================================--*/

function img_product($name_product){

  $stmt = Conexion::conn()->prepare("SELECT * FROM tc_products WHERE id = :id");

      $stmt -> bindParam(":id", $name_product, PDO::PARAM_INT);

      $stmt -> execute();

      if ($f = $stmt->fetch(PDO::FETCH_ASSOC)) {
        return $f['image'];
      }
 
    $stmt = null;
  
}

/*--=============================================
FUNCIONES PARA LOS PRODUCTOS
==============================================--*/

function url_product($id_product){

  $stmt = Conexion::conn()->prepare("SELECT * FROM tc_products WHERE id = :id");

      $stmt -> bindParam(":id", $id_product, PDO::PARAM_INT);

      $stmt -> execute();

      if ($f = $stmt->fetch(PDO::FETCH_ASSOC)) {
        return $f['url'];
      }
 
    $stmt = null;
  
}

/*--=============================================
FUNCIONES PARA LOS PRODUCTOS
==============================================--*/

function nae_p($nae_p){

  $stmt = Conexion::conn()->prepare("SELECT * FROM tc_products WHERE url = :url");

      $stmt -> bindParam(":url", $nae_p, PDO::PARAM_INT);

      $stmt -> execute();

      if ($f = $stmt->fetch(PDO::FETCH_ASSOC)) {
        return $f['name'];
      }
 
    $stmt = null;
  
}

/*--=============================================
FUNCIONES PARA LOS USUARIOS
==============================================--*/

function userD($item, $userD){

  if ($item == 'userid') {
        $stmt = Conexion::conn()->prepare("SELECT * FROM tc_users WHERE username = :username");

        $stmt -> bindParam(":username", $userD, PDO::PARAM_STR);

        $stmt -> execute();

        if ($f = $stmt->fetch(PDO::FETCH_ASSOC)) {
          return $f['id'];
        }  

  }else if ($item == 'picture') {
      $stmt = Conexion::conn()->prepare("SELECT * FROM tc_users WHERE username = :username");

      $stmt -> bindParam(":username", $userD, PDO::PARAM_STR);

      $stmt -> execute();

      if ($f = $stmt->fetch(PDO::FETCH_ASSOC)) {
        return $f['picture'];
      }

  }else if ($item == 'type_login') {
      $stmt = Conexion::conn()->prepare("SELECT * FROM tc_users WHERE username = :username");

      $stmt -> bindParam(":username", $userD, PDO::PARAM_STR);

      $stmt -> execute();

      if ($f = $stmt->fetch(PDO::FETCH_ASSOC)) {
        return $f['type_login'];
      }
  }

  
 
    $stmt = null;
  
}

function id_user($userid){

  $stmt = Conexion::conn()->prepare("SELECT * FROM tc_users WHERE username = :username");

      $stmt -> bindParam(":username", $userid, PDO::PARAM_STR);

      $stmt -> execute();

      if ($f = $stmt->fetch(PDO::FETCH_ASSOC)) {
        return $f['id'];
      }
 
    $stmt = null;
  
}


/*--=============================================
= HORA AGARADABLES
==============================================--*/

function created($fecha) {
  if(empty($fecha)) {
      return "No hay fecha";
  }
   
  $intervalos = array("segundo", "minuto", "hora", utf8_decode("día"), "semana", "mes", utf8_decode("año"));
  $duraciones = array("60","60","24","7","4.35","12");
   
  $ahora = time();
  $Fecha_Unix = strtotime($fecha);
  
  if(empty($Fecha_Unix)) {   
      return "Fecha incorrecta";
  }
  if($ahora > $Fecha_Unix) {   
      $diferencia     =$ahora - $Fecha_Unix;
      $tiempo         = "Hace";
  } else {
      $diferencia     = $Fecha_Unix -$ahora;
      $tiempo         = "Dentro de";
  }
  for($j = 0; $diferencia >= $duraciones[$j] && $j < count($duraciones)-1; $j++) {
    $diferencia /= $duraciones[$j];
  }
   
  $diferencia = round($diferencia);
  
  if($diferencia != 1) {
    $intervalos[5].="e"; //MESES
    $intervalos[$j].= "s";
  }
   
    return "$tiempo $diferencia $intervalos[$j]";
  }


  function date_offer ($fecha) {
  $fecha = substr($fecha, 0, 10);
  $numeroDia = date('d', strtotime($fecha));
  $dia = date('l', strtotime($fecha));
  $mes = date('F', strtotime($fecha));
  $anio = date('Y', strtotime($fecha));
  $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
  $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
  $nombredia = str_replace($dias_EN, $dias_ES, $dia);
$meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
  $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
  $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
  // return /*$nombredia." ".*/$numeroDia." de ".$nombreMes." de ".$anio;
  return $nombreMes . ' ' . $numeroDia . ' ' . $anio;
}
/*--=============================================
MOSTRA MAPA SEGÚN LON Y LTI
==============================================--*/

function getGeocodeData($address) {
  $address = urlencode($address);

  $googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key=AIzaSyAbAG-TPH6II3HSx69mJEsH2Wdp7YSkf-Q&callback";

  $geocodeResponseData = file_get_contents($googleMapUrl);

  $responseData = json_decode($geocodeResponseData, true);
  
  if($responseData['status']=='OK') {
  $latitude = isset($responseData['results'][0]['geometry']['location']['lat']) ? $responseData['results'][0]['geometry']['location']['lat'] : "";
  $longitude = isset($responseData['results'][0]['geometry']['location']['lng']) ? $responseData['results'][0]['geometry']['location']['lng'] : "";
  $formattedAddress = isset($responseData['results'][0]['formatted_address']) ? $responseData['results'][0]['formatted_address'] : "";
  if($latitude && $longitude && $formattedAddress) {
  $geocodeData = array();
  array_push(
  $geocodeData,
  $latitude,
  $longitude,
  $formattedAddress
  );
  return $geocodeData;
  } else {
  return false;
  }
  } else {
  echo "ERROR: {$responseData['status']}";
  return false;
  }
}

function estado($s)
{
  if ($s=='0') {
    return '<button class="btn btn-xs btn-danger">Pendiente de aprobación</button>';
  }
  else if ($s=='1') {
    return '<button class="btn btn-xs btn-success">Aprobado</button>';
  }

}

function paque($s)
{
  if ($s=='editor') {
    return 'Activo';
  }

}

/*--=============================================
TRAER DATOS DE USUARIO
==============================================--*/


function validate_session()
{
  require_once APPPATH. ('/controllers/urls.ctr.php');

  $urls = URL::ctrUrl();

  if (isset($_SESSION['token_access']) && isset($_SESSION['id']) && isset($_SESSION['validate'])) {

  if ($_SESSION['validate'] == "ok") {
    $item = "id";
    $value = $_SESSION['id'];
     $view_user = CtrUsers::ctrViewUser($item, $value);

     if ($_SESSION['token_access'] != $view_user['token_access']) {
         echo '<script>
          
              window.location = "'.$urls.'";

          </script>';

          exit();
     }


  }else {
     echo '<script>
          
              window.location = "'.$urls.'";

          </script>';

          exit();
  }

}else {
   echo '<script>
          
              window.location = "'.$urls.'";

          </script>';

          exit();
}
}

/*=============================================
SI HAY COOKIES INICIAMOS SESSION
=============================================*/

if(isset($_COOKIE['idcookie']) && isset($_COOKIE['namecookie']) && !isset($_SESSION['id'])){

  
  $_SESSION['id']=$_COOKIE['idcookie'];
  $_SESSION['username']=$_COOKIE['namecookie'];


}