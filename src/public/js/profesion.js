$( document ).ready(function() {

    
    $("#nuevaProfesion").on('click',  function(event) {
        event.preventDefault();
    
        var nameProfesion = $("#nombreProfesion").val();
        var ramaProfesion = $("#ramaProfesion").val();

        var url = $("#url").val();

        if (nameProfesion =='') {
            swal('Error', 'Error nombre de la profesion vacia!', 'error');
      
            return false;
        };
        if (ramaProfesion =='') {
            swal('Error', 'Errorrama de la profesion vacia!', 'error');
        
            return false;
        };
        
        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/profesion_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    swal({
                        title: "¡OK!",
                        text: "Profesion Registrada exitosamente!",
                        type: "success",
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    },
                    
                    function(isConfirm) {
                        if (isConfirm) {
                            window.location.reload();
                    
                        }
                    });
                }else if (response == "exist") {

					swal('Error', 'Error ya esta Profesion esta registrada!', 'error');

				};
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("registerProfesion=true&nameProfesion="+ nameProfesion +"&ramaProfesion="+ ramaProfesion);
    
    })

    $(".statusProfesion").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();

       var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/profesion_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    var st = $("#profesionStatus_"+id).attr("class");
                    if(st == "badge badge-danger"){
                      
                        $("#profesionStatus_"+id).attr("class","badge badge-success");
                        $("#profesionStatus_"+id).text("Activo");
               

                        $("#statusPrefesion_"+id).addClass("btn-danger");
                        $("#statusPrefesion_"+id).removeClass("btn-success");
                        $("#statusPrefesion_"+id).text("Inhabilitar ");
                 

                    }else if(st == "badge badge-success"){

                        $("#profesionStatus_"+id).attr("class","badge badge-danger");
                        $("#profesionStatus_"+id).text("Inactivo");
                                        

                        $("#statusPrefesion_"+id).addClass("btn-success");
                        $("#statusPrefesion_"+id).removeClass("btn-danger");
                        $("#statusPrefesion_"+id).text("Habilitar");
                        

                    }
                    console.log(st);
                }else if(response == "error"){
                    console.log("erro");
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("statusProfesion=true&status="+ id);

    })

    $(".getProfesion").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();
        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/profesion_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response !== "") {
                    var obj = jQuery.parseJSON(response);
                    $("#idProfesion").val(obj.id);
                    $("#nombreProfesion").val(obj.nombre);
                    $("#ramaProfesion").val(obj.rama);
                   
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("getProfesionId=true&id="+ id);
    })

    $("#editProfesion").on('click',  function(event) {

        var profesionId= $("#idProfesion").val();
        var profesionNombre= $("#nombreProfesion").val();
        var profesionRama = $("#ramaProfesion").val();
        var url = $("#url").val();
        if (profesionNombre =='') {
            swal('Error', 'Error nombre de la profesion vacia!', 'error');
      
            return false;
        };
        if (profesionRama =='') {
            swal('Error', 'Errorrama de la profesion vacia!', 'error');
        
            return false;
        };

        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/profesion_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    swal({
                        title: "¡OK!",
                        text: "¡Profesion Editada exitosamente!",
                        type: "success",
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    },
                    
                    function(isConfirm) {
                        if (isConfirm) {
                            window.location.reload();
                    
                        }
                    });
                }else if (response == "exist") {

					swal('Error', 'Error ya esta profesion existe!', 'error');

				};
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("editProfesion=true&profesionId="+ profesionId +"&profesionNombre="+ profesionNombre +"&profesionRama="+ profesionRama);
    

    })

    function EsEmail(w_email) {
    
        var test = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var emailReg = new RegExp(test);
    
        return responseTestEmail = emailReg.test(w_email);
    }
    
        
});
