<?php

require_once '../controllers/horario_controller.php';
require_once '../models/horario_model.php';

require_once '../controllers/curso_grupal_controller.php';
require_once '../models/curso_grupal_model.php';

require_once '../controllers/profesor_controller.php';
require_once '../models/profesor_model.php';


 
if (isset($_POST["registerHorario"])){

    $horario = horarioController::createHoario($_POST);
   echo $horario;
}
	
if (isset($_POST["statusHorario"])){

    $status_admin = horarioController::statusHorario($_POST);
   echo $status_admin;
}

if (isset($_POST["getDatesProfesion"])){

    $curso_grupal_by_profesion = cursoGrupalModel::getCursoGrupalByProfesion($_POST);
    $profesor_by_profesion = profesorModel::getProfesorByProfesion($_POST);
    if(empty($curso_grupal_by_profesion)){
        echo "no_curso";
    }else if(empty( $profesor_by_profesion )){
        echo "no_profesor";
    }else{
       
        $array = ["cursos" => $curso_grupal_by_profesion ,"profesores" => $profesor_by_profesion];
   
        echo json_encode($array);
    }
    
   
}

if (isset($_POST["getHoraioId"])){

    $horario_by_id = horarioController::getHorarioById($_POST);
    $profesor = ["id" => $horario_by_id["profesor_id"]];
    $curso = ["id" => $horario_by_id["curso_id"]];
   
    $profesor_by_id = profesorController::getProfesorById($profesor);
    $curso_by_id = cursoGrupalModel::getCursoGrupalId($curso);
   
    $array = ["id"=>$horario_by_id["id"],"hora_inicio"=>$horario_by_id["hora_inicio"],"hora_finalizar"=>$horario_by_id["hora_finalizar"],"diasemana"=>$horario_by_id["diasemana"],"numero_cleintes"=>$horario_by_id["numero_cleintes"],"link_zoom"=>$horario_by_id["link_zoom"],"profesor_id"=>$profesor_by_id["id"],"nombre_profesor"=>$profesor_by_id["nombre"],"curso_id"=>$curso_by_id["id"],"nombre_curso"=>$curso_by_id["nombre"]];
    
   echo json_encode($array);
}



if (isset($_POST["editHorario"])){

    $edit_horario = horarioController::editHorario($_POST);
    
   echo $edit_horario;
}

?>