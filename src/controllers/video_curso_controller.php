<?php
class videoCursoController{

    public static function allCursos(){

        $prefesiones = new videoCursoModel();
        $all_prefesiones = $prefesiones -> getAllCursos();
        return $all_prefesiones;
    }

    public function getCursoById($post){

        $profesores = new videoCursoModel();
        $profesor_id = $profesores -> getCursoId($post);
        return $profesor_id;
    }

    public function editVideoCurso($post){

        $cursoVideo = new videoCursoModel();

        $getCursoVideo = $cursoVideo -> getvideoCurso($post);

        if ($getCursoVideo['id'] == $post['id_video_curso'] || empty($getCursoVideo)){

           $video_curso_id = $cursoVideo -> cursoEdit($post);

            if ($video_curso_id == "ok"){
                echo "ok";
            }else {
                echo "error";
            }
        }else{
            echo "exist";
        } 
    }

    public function statusVideoCurso($post){

        $VideoCurso = new videoCursoModel();
        $video = $VideoCurso -> statusVideoCurso($post);
        return $video;
    }
     
    public function createVideoCurso($post){

        $cursoNew = new videoCursoModel();
        $curso = $cursoNew -> getVideoCurso($post);
        if ($curso == "" && $curso == null && $curso == false){
            $register = $cursoNew -> newCurso($post);
            if ($register == "ok"){
                echo "ok";
            }else {
                echo "error";
            }
        }else{
            echo "exist";
        }    
    }
 
}



?>
