$( document ).ready(function() {

 
    $("#registerProfesor").on('click',  function(event) {
        event.preventDefault();

        for (instance in CKEDITOR.instances) 
        {
            CKEDITOR.instances[instance].updateElement();
        }
    
        var nameProfesor = $("#nameProfesor").val();
        var apellidoProfesor = $("#apellidoProfesor").val();
        var mailProfesor = $("#mailProfesor").val();
        var passProfesor = $("#passProfesor").val();
        var botonFtf = $("#botonFtf").val();
        var img = $("#imgPerson").val();
        var especialidad = $("#especialidad").val();
        var profesionProfesor = $("#profesionProfesor").val();
        var url = $("#url").val();
        var formData = new FormData($("#form_teacher")[0]);
        if (nameProfesor =='') {
            swal('Error', 'Error nombre vacio!', 'error');
        
            return false;
        };
        if (apellidoProfesor =='') {
            swal('Error', 'Error nombre vacio!', 'error');
        
            return false;
        }; 
        if (mailProfesor =='') {
            swal('Error', 'Error correo vacio!', 'error');
            return false;
        }else {
            var mail = EsEmail(mailProfesor);
            if(mail == false){
                swal('Error', 'Error correo no correcto!', 'error');
                return false;
            }
        };
        if (passProfesor =='') {
            swal('Error', 'Error contraseña vacio!', 'error');
        
            return false;
        };
        if (profesionProfesor =='') {
            swal('Error', 'Error elija una profesion para este profesor!', 'error');
        
            return false;
        };
        if (botonFtf =='') {
            swal('Error', 'El boton Face to Face es muy importante, ingrese uno!', 'error');
        
            return false;
        };
        if (especialidad =='') {
            swal('Error', 'Especialidad vacia!', 'error');
        
            return false;
        };
        if (img =='') {
            swal('Error', 'Foto del instructor vacia!', 'error');
        
            return false;
        };

        $.ajax({
            type: 'POST',
            url: url +'src/ajax/profesor_ajax.php',
            data: formData,
            contentType: false,
            processData: false,
            success: function(response) {

                var obj = jQuery.parseJSON(response);
         
                swal({
                    title: "¡"+obj.mensaje+"!",
                    text: obj.contenido,
                    type: obj.type,
                    confirmButtonText: "Cerrar",
                    closeOnConfirm: false
                },
                
                function(isConfirm) {
                    if (isConfirm) {
                        window.location.reload();
                
                    }
                });
            }
           
        })

    })

    $(".statusProf").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();

       
        $.ajax({
            type: "POST",
            url: url +'src/ajax/profesor_ajax.php',
            data: {statusProf:true, status:id}
            ,
            success: function(response) {
           
                  
                    var st = $("#profStatus_"+id).attr("class");
                    if(st == "badge badge-danger"){
                      
                        $("#profStatus_"+id).attr("class","badge badge-success");
                        $("#profStatus_"+id).text("Activo");
                        
                     
                        $("#statusProf_"+id).addClass("btn-danger");
                        $("#statusProf_"+id).removeClass("btn-success");
                        $("#statusProf_"+id).text("Inhabilitar ");
                 

                    }else if(st == "badge badge-success"){

                        $("#profStatus_"+id).attr("class","badge badge-danger");
                        $("#profStatus_"+id).text("Inactivo");
                 

                        $("#statusProf_"+id).addClass("btn-success");
                        $("#statusProf_"+id).removeClass("btn-danger");
                        $("#statusProf_"+id).text("Habilitar");
                        

                    }
            
               
            }
        });

                
        
    })

    $(".getProf").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();
        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/profesor_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response !== "") {
                    var obj = jQuery.parseJSON(response);
                    $("#idProfesor").val(obj.id);
                    $("#nameProfesor").val(obj.nombre);
                    $("#apellidoProfesor").val(obj.apellido);
                    $("#mailProfesor").val(obj.mail);
                    $("#passProfesor").val(obj.password);
                    $("#descripcionProfesor").val(obj.descripcion);
                    $("#botonFtf").val(obj.boton_ftf);
                    $("#imgPersonHidden").val(obj.img);
                    $("#especialidad").val(obj.especialidad);
                    $("#profesionProfesor").val(obj.profesion_id);
        
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("getProfesorId=true&id="+ id);
    })

    $( "#imgPerson" ).change(function() {
         var file = document.getElementById('imgPerson').files[0].name;
        $("#imgPersonHidden").val(file);
      });

    $("#editProfesor").on('click',  function(event) {
        event.preventDefault();

       
        

        for (instance in CKEDITOR.instances) 
        {
            CKEDITOR.instances[instance].updateElement();
        }
        var idProfesor  = $("#idProfesor").val();
        var nameProfesor = $("#nameProfesor").val();
        var apellidoProfesor = $("#apellidoProfesor").val();
        var mailProfesor = $("#mailProfesor").val();
        var passProfesor = $("#passProfesor").val();
        var botonFtf = $("#botonFtf").val();
        var img = $("#imgPersonHidden").val();
        var especialidad = $("#especialidad").val();
        var profesionProfesor = $("#profesionProfesor").val();
        var url = $("#url").val();
        var formData = new FormData($("#form_teacher")[0]);
        if (nameProfesor =='') {
            swal('Error', 'Error nombre vacio!', 'error');
        
            return false;
        };
        if (apellidoProfesor =='') {
            swal('Error', 'Error nombre vacio!', 'error');
        
            return false;
        };
        if (mailProfesor =='') {
           
            swal('Error', 'Error correo vacio!', 'error');
            return false;
        }else { 
            var mail = EsEmail(mailProfesor);
            if(mail == false){
                swal('Error', 'Error correo no correcto!', 'error');
                return false;
            }
        };
        if (passProfesor =='') {
            swal('Error', 'Error contraseña vacio!', 'error');
        
            return false;
        };
        if (profesionProfesor =='') {
            swal('Error', 'Error elija una profesion para este profesor!', 'error');
        
            return false;
        };
        if (botonFtf =='') {
            swal('Error', 'El boton Face to Face es muy importante, ingrese uno!', 'error');
        
            return false;
        };
        if (especialidad =='') {
            swal('Error', 'Especialidad vacia!', 'error');
        
            return false;
        };
        if (img =='') {
            swal('Error', 'Foto del instructor vacia!', 'error');
        
            return false;
        };

        $.ajax({
            type: 'POST',
            url: url +'src/ajax/profesor_ajax.php',
            data: formData,
            contentType: false,
            processData: false,
            success: function(response) {

                var obj = jQuery.parseJSON(response);
         
                swal({
                    title: "¡"+obj.mensaje+"!",
                    text: obj.contenido,
                    type: obj.type,
                    confirmButtonText: "Cerrar",
                    closeOnConfirm: false
                },
                
                function(isConfirm) {
                    if (isConfirm) {
                        window.location.reload();
                
                    }
                });
            }
           
        })

    })

    function EsEmail(w_email) {
    
        var test = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var emailReg = new RegExp(test);
    
        return responseTestEmail = emailReg.test(w_email);
    }
        
});
