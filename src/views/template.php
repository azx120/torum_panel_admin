<?php 
session_start();
$urls = URL::ctrUrl(); 
$UrlPortal = URL::ctrUrlPortal(); 
$routes = [];
if (isset($_GET['urls'])) {

  $routes = explode("/", $_GET["urls"]);
    /*==============================
      MOSTRANDO TITULO DE RUTAS
      ==============================*/
    if (isset($routes[0])) {

      $route = $routes[0];

    }else if (isset($routes[1])) {

      $route = $routes[1];

    }else{
      $route = $routes[0];
    }

}else{
    $route  = "home";
}

 ?>
<!DOCTYPE html>
<html lang="es">
<head>

	<meta charset="utf-8">
  <link rel="icon" type="image/png" href="<?php echo $UrlPortal; ?>src/public/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

      <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo $urls; ?>src/public/css/plugins/fontawesome-free/css/all.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo $urls; ?>src/public/css/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $urls; ?>src/public/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo $urls; ?>src/public/css/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <link rel="stylesheet" href="<?php echo $urls; ?>src/public/plugins/sweetalert/sweetalert.css">

  <link rel="stylesheet" href="<?php echo $urls; ?>src/public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo $urls; ?>src/public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">


  <link rel="stylesheet" href="<?php echo $urls; ?>src/public/plugins/player/css/plyr.css">

  <script src="<?php echo $urls; ?>src/public/plugins/player/js/plyr.min.js"></script>

  <!--<link rel="stylesheet" href="./src/public/css/style_login.css">-->
 
</head>

<body class="hold-transition sidebar-mini">

  <?php 

  if (isset($_SESSION['validate']) && $_SESSION['validate'] == 'ok' /*&& isset($_SESSION['token_access'])*/) {


  echo '<div class="wrapper">';

  /*=============================================
   HEADER
   =============================================*/
    include ('includes/header.php');

  /*=============================================
   ASIDE
   =============================================*/

    include ('includes/aside.php');

    $routes = [];
    $route = null;
    
    
        if (isset($_GET['urls'])) {
          $routes = explode("/", $_GET['urls']);
          if($routes[0] == "clientes"             ||
            $routes[0] == "profesores"            ||
            $routes[0] == "close"                 ||
            $routes[0] == "forgot-password"       ||
            $routes[0] == "home"                  ||
            $routes[0] == "crear-profesion"       ||
            $routes[0] == "profesiones"           ||
            $routes[0] == "lista_pagos"           ||
            $routes[0] == "listaCurso"            ||
            $routes[0] == "cursos"                ||
            $routes[0] == "cursos_grupal"         ||
            $routes[0] == "register_curso_grupal" ||
            $routes[0] == "listaCursoFtF" ||
            $routes[0] == "register_curso"        ||
            $routes[0] == "clientes_cursando_plus"||
            $routes[0] == "upload_videos"         ||
            $routes[0] == "videos"                ||
            $routes[0] == "video_curso"           ||
            $routes[0] == "register_horario"      ||
            $routes[0] == "register_horario_ftf"  ||
            $routes[0] == "horarios"              ||
            $routes[0] == "horarios_ftf"          ||
            $routes[0] == "recover-password"      ||
            $routes[0] == "register_admin"        ||
            $routes[0] == "admins"                ||
            $routes[0] == "register_teacher" ) {

            include ('pages/'.$routes[0].'.php');
         }else {
          include ('pages/404.php');
         }
        }else{

         include ('pages/home.php');
          
       }
      
      
  /*=============================================
   FOOTER
   =============================================*/

    //include ('pages/footer.php');

    echo "</div><!-- ./wrapper -->";
}else{

echo '<body class="hold-transition sidebar-mini login-page bg-dark">';
// MOSTRAMOS EL FORMULARIO SEGÚN LA OPCION A O P
$routes = [];
if (isset($_GET['urls'])) {
  $routes = explode("/", $_GET['urls']);
      if (isset($routes[0])) {
        if ($routes[0] == "anunciante" || $routes[0] == "proveedor") {
          include ('pages/register.php');
        }
      }else{
        include ('pages/login.php');
      }
}else{
  include ('pages/login.php');
  
}


}

 ?>
  <input type="hidden" value="<?php echo $urls ?>" id="url">
  <!-- jQuery -->
<script src="<?php echo $urls; ?>src/public/js/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo $urls; ?>src/public/js/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 4 -->
<script src="<?php echo $urls; ?>src/public/js/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo $urls; ?>src/public/css/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo $urls; ?>src/public/js/adminlte.js"></script>

<script src="<?php echo $urls; ?>src/public/js/user_admin.js"></script>
<script src="<?php echo $urls; ?>src/public/js/users.js"></script>
<script src="<?php echo $urls; ?>src/public/js/profesion.js"></script>
<script src="<?php echo $urls; ?>src/public/js/profesor.js"></script>
<script src="<?php echo $urls; ?>src/public/js/cursos.js"></script>
<script src="<?php echo $urls; ?>src/public/js/videos_curso.js"></script>
<script src="<?php echo $urls; ?>src/public/js/cursos_grupal.js"></script>
<script src="<?php echo $urls; ?>src/public/js/horarios.js"></script>
<script src="<?php echo $urls; ?>src/public/js/horarios_ftf.js"></script>


<script src="<?php echo $urls; ?>src/public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo $urls; ?>src/public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo $urls; ?>src/public/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo $urls; ?>src/public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<script src="<?php echo $urls; ?>src/public/plugins/sweetalert/sweetalert.min.js"></script>


<script src="<?php echo $urls; ?>src/public/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace("editor1");
</script>

<script>
  $(function () {
    $('#dataTablePanel').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
    $('#dataTablePanel2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
    $('#dataTablePanelNot').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": false,
    });
  });

</script>



	
</body>
</html>