<?php 
    $id = ["id"=>$_GET["id"]];
    $curso_by_id = cursoModel::getCursoId($id);
    $video_curso_by_id = videoCursoModel::getAllVideosCurso($id);
    $curso_id = ["profesion_id"=>$curso_by_id["profesion_id"]];
    
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Videos Del Curo  <?php echo $curso_by_id["nombre"]; ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo $urls; ?>">Home</a></li>
              <li class="breadcrumb-item active">Projects</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Projects</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects dataTablePanel">
              <thead>
                  <tr>
                      <th style="width: 5%">
                          #
                      </th>
                      <th >
                          Titulo
                      </th>
                      <th style="width: 10%">
                         Nro. Del Videos
                      </th>
                      <th  >
                          Video Asignado
                      </th>
                      <th>
                          Status
                      </th>
                      <th style="width: 20%">
                        Acciones
                      </th>
                  </tr>
              </thead>
              <tbody>
              <?php foreach($video_curso_by_id as $video_curso):
                
                if($video_curso['status'] == 0){
                  $status = "Inactivo";
                  $status_button = "btn-success";
                  $status_class = "badge badge-danger";
                  $content_button = "Habilitar"; 
              }else{
                  $status = "Activo";
                  $status_button = "btn-danger";
                  $status_class = "badge badge-success";
                  $content_button = "Inabilitar";
              }
              ?>
              
                <tr>
                    <td>
                      <p id="id_video_<?php echo $video_curso['id']; ?>" value="<?php echo $video_curso['id']; ?>">
                        #
                      </p>  
                    </td>
                    <td>
                      <p id="tituloVideo_<?php echo $video_curso['id']; ?>" value="<?php echo $video_curso['titulo']; ?>">
                        <?php echo $video_curso['titulo']; ?>
                      </p>
                      
                    </td>
                    <td>
                        <p id="nroVideo_<?php echo $video_curso['id']; ?>">
                            <?php echo $video_curso['nro_video']; ?>
                        </p>
            
                    </td>
                
                    <td class="project_progress" id="videoId_<?php echo $video_curso['id']; ?>" value="">
                        <p id="video_id_<?php echo $video_curso['id']; ?>"><?php echo $video_curso['video_id']; ?></p>
                 
                      </p>
                    </td>
                    <td >
                        <span id="videoCursoStatus_<?php echo $video_curso['id']; ?>" class="<?php echo $status_class; ?>"> <?php echo $status; ?></span>
                    </td>
                    <td class="project-actions">
                    
                        <button class="btn btn-info btn-sm getVideoCurso" data-toggle="modal" data-target="#exampleModalCenter" value="<?php echo $video_curso['id']; ?>">
                            <i class="fas fa-pencil-alt">
                            </i>
                            Editar
                        </button>
                        <button class="btn <?php echo $status_button ?> btn-sm statusVideoCurso" id="statusVideoCurso_<?php echo $video_curso['id']; ?>" value="<?php echo $video_curso['id']; ?>">
                            <i class="fas fa-trash">
                            </i>
                            <?php echo $content_button ?> 
                        </button>
                    </td>
                </tr>

            <?php endforeach; ?>
              </tbody>
          </table>
        </div> 
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
       <!-- Default box -->
       <div class="card mt-4">
        <div class="card-header">
          <h2 class="center" align="center">Registrar/Editar Videos Del Curso</h2>
          <form method="post">
            <input type="hidden" class="form-control" name="" id="id_curso" value="<?php echo $curso_by_id["id"]; ?>">
            <input type="hidden" class="form-control" name="" id="register_video_curso" value="true">
            <input type="hidden" class="form-control" name="" id="id_video_curso">
            <div class="input-group mb-3">
              <input type="text" class="form-control" name="" id="titleVideoCurso" placeholder="Titulo Del Del video Para El Curso">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="number" class="form-control" name="" id="nroVideosCurso" placeholder="Numero del video En El Curso" min="1" max="<?php echo $curso_by_id["numero_videos"]; ?>">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="text" class="form-control" name="" id="idVideo" placeholder="video Asignado">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
             <button type="button" class="btn btn-danger ml-4" onClick="window.location.reload();">Vaciar</button>
             <p class="ml-4" style="color:red;" id="mensaje_form"></p>
            </div>     
            <div class="row">
                <!-- /.col -->
              <div class="col-12">
                <button type="submit" class="btn btn-primary btn-block" id="sendVideoCurso" >Registrar</button>
              </div>
                <!-- /.col -->
            </div>        
          </form>
        </div> 
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


 