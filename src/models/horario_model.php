<?php
require_once "conexion.php";

class horarioModel{

	//crear nuevo usuario
	public function getAllHorarios(){

		$stmt = Conexion::conection()->prepare("SELECT * FROM horarios");

		$stmt->execute();
		return $stmt->fetchall();

	}

	//crear nuevo usuario
	public static function getAllClientsHorarios($post){

		$stmt = Conexion::conection()->prepare("SELECT clientes.id, nombre, apellido,mail,telefono,cursando_grupal.status,cursando_grupal.id as id_grupal FROM clientes INNER JOIN cursando_grupal ON clientes.id = cursando_grupal.user_id WHERE cursando_grupal.horario_id = :horario_id");
		$stmt->bindParam(":horario_id", $post["id"], PDO::PARAM_INT);

		$stmt->execute();
		return $stmt->fetchall();

	}

	public function statusHorario($post){

		$stmt = Conexion::conection()->prepare("UPDATE horarios SET status = NOT status WHERE id = :id");
		$stmt->bindParam(":id", $post["status"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

	}

	public function editHorario($post){
		

		$stmt = Conexion::conection()->prepare("UPDATE horarios SET hora_inicio = :hora_inicio, hora_finalizar = :hora_finalizar,link_zoom = :link_zoom, profesor_id = :profesor_id,curso_id = :curso_id,numero_cleintes=:numero_cleintes,diasemana=:diasemana WHERE id = :id");
		
		$stmt->bindParam(":id", $post["idHorario"], PDO::PARAM_INT);
		$stmt->bindParam(":hora_inicio", $post["hora_inicio"], PDO::PARAM_STR);
		$stmt->bindParam(":hora_finalizar", $post["hora_finalizar"], PDO::PARAM_STR);
		$stmt->bindParam(":link_zoom", $post["linkZoom"], PDO::PARAM_STR);
		$stmt->bindParam(":profesor_id", $post["profesor_id"], PDO::PARAM_INT);
		$stmt->bindParam(":curso_id", $post["curso_id"], PDO::PARAM_INT);
		$stmt->bindParam(":numero_cleintes", $post["nro_client"], PDO::PARAM_STR);
		$stmt->bindParam(":diasemana", $post["diasemana"], PDO::PARAM_STR);
	
			
		if($stmt -> execute()){

			return "ok";	
		
		}else{

			return "error";	

		}

	}
	
	//crear nuevo curso
	public function newHorario($post){
		
		$stmt = Conexion::conection()->prepare("INSERT INTO horarios (hora_inicio, hora_finalizar,diasemana, link_zoom, profesor_id,curso_id,numero_cleintes, status) VALUES (:hora_inicio, :hora_finalizar,:diasemana,:link_zoom, :profesor_id,:curso_id,:numero_cleintes, 1)");
		
		$stmt->bindParam(":hora_inicio", $post["hora_inicio"], PDO::PARAM_STR);
		$stmt->bindParam(":hora_finalizar", $post["hora_finalizar"], PDO::PARAM_STR);
		$stmt->bindParam(":link_zoom", $post["linkZoom"], PDO::PARAM_STR);
		$stmt->bindParam(":profesor_id", $post["profesor_id"], PDO::PARAM_INT);
		$stmt->bindParam(":curso_id", $post["curso_id"], PDO::PARAM_INT);
		$stmt->bindParam(":numero_cleintes", $post["nro_client"], PDO::PARAM_STR);
		$stmt->bindParam(":diasemana", $post["diasemana"], PDO::PARAM_STR);
	

		if($stmt->execute()){
			return "ok";
		}else{
			return "error";
		}
	}
	
	
	//Metodos de consulta
	public static function getHorarioById($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM horarios WHERE id = :id");

		$stmt -> bindParam(":id", $post['id'], PDO::PARAM_INT);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}

    
    //Metodos de consulta
	public function getHorarioProfesor($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM cursos WHERE profesor_id = :profesor_id");

		$stmt -> bindParam(":profesor_id", $post['profesor_id'] , PDO::PARAM_INT);
		$stmt -> bindParam(":hora_inicio", $post['hora_inicio'] , PDO::PARAM_STR);
		$stmt -> bindParam(":hora_finalizar", $post['hora_finalizar'] , PDO::PARAM_STR);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;
 
	}

		//crear nuevo usuario
		public static function getAllcursosHorarios(){ 

			$stmt = Conexion::conection()->prepare("SELECT horarios.id,horarios.profesor_id, cursos_grupal.nombre FROM cursos_grupal INNER JOIN horarios ON horarios.curso_id = cursos_grupal.id");

	
			$stmt->execute();
			return $stmt->fetchall();
	
		}

}
?>
