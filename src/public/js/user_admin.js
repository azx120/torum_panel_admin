$( document ).ready(function() {

    $("#login").on('click',  function(event) {
        event.preventDefault();
    
        var user = $("#usernameLogin").val();
        var pass = $("#password").val();
        var url = $("#url").val();

        if (user == '' || pass == '') {

            swal('Error', 'Hay un campo vacio', 'error');
      
            return false;
        };
              
        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/user_admin_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {

                    window.location.reload();
                }else if(response == "error"){
                    swal('Error', 'Datos incorrectos', 'error');
                }else if(response == "disabled"){
                    swal('Error', 'Este usuario esta deshabilitado!', 'error');
                };
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("usernameLogin="+ user +"&password="+ pass);
    

    })
    
    $("#registerAdmin").on('click',  function(event) {
        event.preventDefault();
    
        var nameUserADmin = $("#nameUserADmin").val();
        var nameAdmin = $("#nameAdmin").val();
        var mailAdmin = $("#mailAdmin").val();
        var passAdmin = $("#passAdmin").val();
        var rolAdmin = $("#rolAdmin").val();
        var url = $("#url").val();

        if (nameUserADmin =='') {
            swal('Error', 'Error nombre de usuario vacio!', 'error');
      
            return false;
        };
        if (nameAdmin =='') {
            swal('Error', 'Error nombre vacio!', 'error');
        
            return false;
        };
        if (mailAdmin =='') {
            swal('Error', 'Error correo vacio!', 'error');
            return false;
        }else {
            var mail = EsEmail(mailAdmin);
            if(mail == false){
                swal('Error', 'Error correo no correcto!', 'error');
                return false;
            }
        };
        if (passAdmin =='') {
            swal('Error', 'Error contraseña vacio!', 'error');
        
            return false;
        };
        if (rolAdmin =='') {
            swal('Error', 'Error elija un roll para este usuario!', 'error');
        
            return false;
        };

        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/user_admin_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    swal({
                        title: "¡OK!",
                        text: "¡Administrador Registrado exitosamente!",
                        type: "success",
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    },
                    
                    function(isConfirm) {
                        if (isConfirm) {
                            window.location.reload();
                    
                        }
                    });
                }else if (response == "exist") {

					swal('Error', 'Error ya este usuario o correo esta registrado!', 'error');

				};
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("registerAdmin=true&nameUserADmin="+ nameUserADmin +"&nameAdmin="+ nameAdmin + "&mailAdmin=" +mailAdmin + "&passAdmin="+ passAdmin + "&rolAdmin=" + rolAdmin);
    

    })

    $(".statusAdmin").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();

       var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/user_admin_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    var st = $("#adminStatus_"+id).attr("class");
                    if(st == "badge badge-danger"){
                      
                        $("#adminStatus_"+id).attr("class","badge badge-success");
                        $("#adminStatus_"+id).text("Activo");
                        
                     
                        $("#statusAdmin_"+id).addClass("btn-danger");
                        $("#statusAdmin_"+id).removeClass("btn-success");
                        $("#statusAdmin_"+id).text("Inhabilitar ");
                 

                    }else if(st == "badge badge-success"){

                        $("#adminStatus_"+id).attr("class","badge badge-danger");
                        $("#adminStatus_"+id).text("Inactivo");
                 

                        $("#statusAdmin_"+id).addClass("btn-success");
                        $("#statusAdmin_"+id).removeClass("btn-danger");
                        $("#statusAdmin_"+id).text("Habilitar");
                        

                    }
                    console.log(st);
                }else if(response == "error"){
                    console.log("erro");
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("statusAdmin=true&status="+ id);

    })

    $(".getAdmin").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();
        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/user_admin_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response !== "") {
                    var obj = jQuery.parseJSON(response);
                    $("#idUserADmin").val(obj.id);
                    $("#nameUserADmin").val(obj.username);
                    $("#nameAdmin").val(obj.name);
                    $("#mailAdmin").val(obj.mail);
                    $("#rolAdmin").val(obj.rol);
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("getAdminId=true&id="+ id);
    })

    $("#editAdmin").on('click',  function(event) {

        var idUserADmin = $("#idUserADmin").val();
        var nameUserADmin = $("#nameUserADmin").val();
        var nameAdmin = $("#nameAdmin").val();
        var mailAdmin = $("#mailAdmin").val();
        var passAdmin = $("#passAdmin").val();
        var rolAdmin = $("#rolAdmin").val();
        var url = $("#url").val();

        if (nameUserADmin =='') {
            swal('Error', 'Error nombre de usuario vacio!', 'error');
      
            return false;
        };
        if (nameAdmin =='') {
            swal('Error', 'Error nombre vacio!', 'error');
        
            return false;
        };
        if (mailAdmin == '') {
           
            swal('Error', 'Error correo vacio!', 'error');
    
            return false;
        }else {
            var mail = EsEmail(mailAdmin);
            if(mail == false){
                swal('Error', 'Error correo no correcto!', 'error');
                return false;
            }
        };
        if (passAdmin =='') {
            swal('Error', 'Error contraseña vacio!', 'error');
        
            return false;
        };
        if (rolAdmin =='') {
            swal('Error', 'Error elija un roll para este usuario!', 'error');
        
            return false;
        };

        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/user_admin_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    swal({
                        title: "¡OK!",
                        text: "¡Administrador Editado exitosamente!",
                        type: "success",
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    },
                    
                    function(isConfirm) {
                        if (isConfirm) {
                            window.location.reload();
                    
                        }
                    });
                }else if (response == "exist") {

					swal('Error', 'Error ya este usuario o correo ya existe!', 'error');

				};
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("editAdmin=true&idAdmin="+ idUserADmin +"&nameUserADmin="+ nameUserADmin +"&nameAdmin="+ nameAdmin + "&mailAdmin=" +mailAdmin + "&passAdmin="+ passAdmin + "&rolAdmin=" + rolAdmin);
    

    })

    function EsEmail(w_email) {
    
        var test = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var emailReg = new RegExp(test);
    
        return responseTestEmail = emailReg.test(w_email);
    }
        
});
