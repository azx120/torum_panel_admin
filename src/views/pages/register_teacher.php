<?php 
  $profesiones = profesionController::getAllProfesionRegister();
 
?> 
<style>
@media (max-width: 600px) {
  .form_editor{
    margin-left:5% !important;
  }
}
</style>
<div class="form_editor" style="margin-left:40%;">  
  <?php
        if(empty($profesiones)){
          echo "<p class='login-box-msg' style='color:red; font-size:20px;'>Profesiones vacias, Debes registrar alguna!</p>";
        }
  ?>
  <div class="register-box">
    
    <h2 align="center"><b>Registro</b> </br>Nuevo Instructor</h2>
    
    <div class="card">
      <div class="card-body register-card-body">
        <p class="login-box-msg">Rellenar todos los datos</p>
   
        <form method="post" id="form_teacher">
          <div class="input-group mb-3">
          <input type="hidden" class="" name="nameProfesor2" id="">
          <input type="hidden" class="" name="registerprofesor" id="" value="true">
            <input type="text" class="form-control" name="nameProfesor" id="nameProfesor" placeholder="Nombre Completo">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="apellidoProfesor" id="apellidoProfesor" placeholder="Apellido Completo">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="email" class="form-control" name="mailProfesor" id="mailProfesor" placeholder="Correo Electronico">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="passProfesor" id="passProfesor" placeholder="Contraseña">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="file" class="form-control-file" name="imgPerson" id="imgPerson" placeholder="Foto">
            <input type="hidden" class="form-control-file" name="imgPersonHidden" id="imgPersonHidden">
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="especialidad" id="especialidad" placeholder="Especialidad Del Profesor">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-graduation-cap"></span>
              </div>
            </div>
          </div>
          <label>Descripcion del Instructor</label>
          <div class="input-group mb-3">
          <textarea class="editor1" name="editor1" id="editor"></textarea>
  
          </div>
          <div class="input-group mb-3">
          
            <textarea class="form-control" name="botonFtf" id="botonFtf" placeholder="Boton de pago Face To  Face"></textarea>
        
          </div> 
          <div class="input-group mb-3">
            <select class="form-control" name="profesionProfesor" id="profesionProfesor" placeholder="Role">
              <option value="">Profesion</opton>
              <?php foreach($profesiones as $profesion): ?>
                <option value="<?php echo $profesion['id'] ?>"><?php echo $profesion['nombre'] ?></opton>
              <?php endforeach; ?>
            </select> 
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-graduation-cap"></span>
              </div>
            </div>
          </div>
          
          <div class="row">
       
            <!-- /.col -->
            <div class="col-12">
              <button type="submit" class="btn btn-primary btn-block" id="registerProfesor">Register</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>
      <!-- /.form-box -->
    </div><!-- /.card -->
  </div>
<!-- /.register-box -->
</div>

