<?php
require_once "conexion.php";

class horarioFtfModel{ 

	//crear nuevo usuario
	public function getAllHorarios(){

		$stmt = Conexion::conection()->prepare("SELECT * FROM horarios_ftf");

		$stmt->execute();
		return $stmt->fetchall();

	}
 
	public function statusHorario($post){

		$stmt = Conexion::conection()->prepare("UPDATE horarios_ftf SET status = NOT status WHERE id = :id");
		$stmt->bindParam(":id", $post["status"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

	}

	public function editHorario($post){
		

		$stmt = Conexion::conection()->prepare("UPDATE horarios_ftf SET hora_inicio = :hora_inicio, hora_finalizar = :hora_finalizar,link_zoom = :link_zoom, profesor_id = :profesor_id,curso_id = :curso_id,numero_cleintes=:numero_cleintes,diasemana=:diasemana WHERE id = :id");
		
		$stmt->bindParam(":id", $post["idHorario"], PDO::PARAM_INT);
		$stmt->bindParam(":hora_inicio", $post["hora_inicio"], PDO::PARAM_STR);
		$stmt->bindParam(":hora_finalizar", $post["hora_finalizar"], PDO::PARAM_STR);
		$stmt->bindParam(":link_zoom", $post["linkZoom"], PDO::PARAM_STR);
		$stmt->bindParam(":profesor_id", $post["profesor_id"], PDO::PARAM_INT);
		$stmt->bindParam(":numero_cleintes", $post["nro_client"], PDO::PARAM_STR);
		$stmt->bindParam(":curso_id", $post["curso_id"], PDO::PARAM_INT);
		$stmt->bindParam(":diasemana", $post["diasemana"], PDO::PARAM_STR);
	
			
		if($stmt -> execute()){

			return "ok";	
		
		}else{

			return "error";	

		}

	}
	
	//crear nuevo curso
	public function newHorario($post){
		
		$stmt = Conexion::conection()->prepare("INSERT INTO horarios_ftf ( hora_inicio, hora_finalizar,diasemana, link_zoom, profesor_id,curso_id,numero_cleintes, status) VALUES (:hora_inicio, :hora_finalizar,:diasemana,:link_zoom, :profesor_id,:curso_id, :nro_client, 1)");
		
		$stmt->bindParam(":hora_inicio", $post["hora_inicio"], PDO::PARAM_STR);
		$stmt->bindParam(":hora_finalizar", $post["hora_finalizar"], PDO::PARAM_STR);
		$stmt->bindParam(":link_zoom", $post["linkZoom"], PDO::PARAM_STR);
		$stmt->bindParam(":profesor_id", $post["profesor_id"], PDO::PARAM_INT);
		$stmt->bindParam(":curso_id", $post["curso_id"], PDO::PARAM_INT);
		$stmt->bindParam(":nro_client", $post["nro_client"], PDO::PARAM_STR);
		$stmt->bindParam(":diasemana", $post["diasemana"], PDO::PARAM_STR);

		if($stmt->execute()){
			return "ok";
		}else{
			return "error";
		}
	}
	
	
	//Metodos de consulta
	public static function getHorarioById($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM horarios_ftf WHERE id = :id");

		$stmt -> bindParam(":id", $post['id'], PDO::PARAM_INT);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

    }
    
    //Metodos de consulta
	public function getHorarioProfesor($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM cursos WHERE profesor_id = :profesor_id");
 
		$stmt -> bindParam(":profesor_id", $post['profesor_id'] , PDO::PARAM_INT);
		$stmt -> bindParam(":hora_inicio", $post['hora_inicio'] , PDO::PARAM_STR);
		$stmt -> bindParam(":hora_finalizar", $post['hora_finalizar'] , PDO::PARAM_STR);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}

		//crear nuevo usuario
		public static function getAllClientsHorarios($post){

			$stmt = Conexion::conection()->prepare("SELECT clientes.id, nombre, apellido,mail,telefono,cursando_ftf.status,cursando_ftf.id as id_ftf FROM clientes INNER JOIN cursando_ftf ON clientes.id = cursando_ftf.user_id WHERE cursando_ftf.horario_id = :horario_id");
			$stmt->bindParam(":horario_id", $post["id"], PDO::PARAM_INT);
	
			$stmt->execute();
			return $stmt->fetchall();
	
		}

		//crear nuevo usuario
		public static function getAllcursosHorariosFtF(){ 

			$stmt = Conexion::conection()->prepare("SELECT horarios_ftf.id,horarios.profesor_id as profesor, cursos_grupal.nombre FROM cursos_grupal INNER JOIN horarios_ftf ON horarios_ftf.curso_id = cursos_grupal.id");

	
			$stmt->execute();
			return $stmt->fetchall();
	
		}

		public function statusCursoFtfCliente($post){
			$stmt = Conexion::conection()->prepare("UPDATE cursando_ftf SET status = NOT status WHERE id = :id");
			$stmt->bindParam(":id", $post["status"], PDO::PARAM_INT);
	
			if($stmt -> execute()){
	
				return "ok";
			
			}else{
	
				return "error";	
	
			}
	
		}

}
?>
