<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/*==================================
  CONTROLADORES          
==================================*/
require_once APPPATH . ('controllers/template.ctr.php');
require_once APPPATH . ('controllers/urls.ctr.php');
require_once APPPATH . ('controllers/user_admin_controller.php');
require_once APPPATH . ('controllers/users_controller.php');
require_once APPPATH . ('controllers/profesion_controller.php');
require_once APPPATH . ('controllers/profesor_controller.php'); 
require_once APPPATH . ('controllers/curso_controller.php');
require_once APPPATH . ('controllers/video_curso_controller.php');
require_once APPPATH . ('controllers/curso_grupal_controller.php');
require_once APPPATH . ('controllers/horario_controller.php');
require_once APPPATH . ('controllers/horario_ftf_controller.php');
require_once APPPATH . ('controllers/notification.ctr.php');
require_once APPPATH . ('controllers/menssages.ctr.php'); 


/*==============================
  PLUGINS           
==============================*/

 require_once BASEPATH . ('functions.php');

/*==================================
 MODELOS            
==================================*/
require_once APPPATH . ('models/template.mdl.php');
require_once APPPATH . ('models/notification.mdl.php');
require_once APPPATH . ('models/menssages.mdl.php');
require_once APPPATH . ('models/user_admin_model.php');
require_once APPPATH . ('models/users_model.php');
require_once APPPATH . ('models/profesion_model.php');
require_once APPPATH . ('models/profesor_model.php');
require_once APPPATH . ('models/horario_model.php');
require_once APPPATH . ('models/horario_ftf_model.php');
require_once APPPATH . ('models/curso_model.php');
require_once APPPATH . ('models/pagos_model.php');
require_once APPPATH . ('models/video_curso_model.php');
require_once APPPATH . ('models/curso_grupal_model.php');
require_once APPPATH . ('models/curso_ftf_model.php');

$template = new CTR_template();
$template -> template();
