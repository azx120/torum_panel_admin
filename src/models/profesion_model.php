<?php
require_once "conexion.php";

class profesionModel{

	//crear nuevo usuario
	public function getAllProfesion(){

		$stmt = Conexion::conection()->prepare("SELECT * FROM profesiones");

		$stmt->execute();
		return $stmt->fetchall();

	}

		//crear nuevo usuario
		public function getAllProfesionRegister(){

			$stmt = Conexion::conection()->prepare("SELECT * FROM profesiones  WHERE status = 1");
	
			$stmt->execute();
			return $stmt->fetchall();
	
		}

	public function statusProfesion($post){
	
		$stmt = Conexion::conection()->prepare("UPDATE profesiones SET status = NOT status WHERE id = :id");
		$stmt->bindParam(":id", $post["status"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

	}

	public function profesionEdit($post){

        $nombre = mb_strtoupper($post["profesionNombre"]);
        $rama =  mb_strtoupper($post["profesionRama"]);

		$stmt = Conexion::conection()->prepare("UPDATE profesiones SET nombre = :nombre, rama = :rama WHERE id = :id");
		
		$stmt->bindParam(":id", $post["profesionId"], PDO::PARAM_INT);
		$stmt->bindParam(":nombre", $nombre, PDO::PARAM_STR);
		$stmt->bindParam(":rama", $rama, PDO::PARAM_STR);
	
			
		if($stmt -> execute()){

			return "ok";	
		
		}else{

			return "error";	

		}

	}
	
	//crear nueva profesion
	public function newProfesion($post){
        $name =  mb_strtoupper($post["nameProfesion"]);
        $rama =  mb_strtoupper($post["ramaProfesion"]);

		$stmt = Conexion::conection()->prepare("INSERT INTO profesiones (nombre, rama, status) VALUES (:nombre, :rama, 1)");

		$stmt->bindParam(":nombre",$name, PDO::PARAM_STR);
		$stmt->bindParam(":rama",$rama, PDO::PARAM_STR);
		

		if($stmt->execute()){
			return "ok";
		}else{
			return "error";
		}
	}
	
	
	//Metodos de consulta
	public function getProfesionId($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM profesiones WHERE id = :id");

		$stmt -> bindParam(":id", $post['id'], PDO::PARAM_INT);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

    }
    
    //Metodos de consulta
	public function getPrfesion($post){
        $name = mb_strtoupper($post['nameProfesion']);

		$stmt = Conexion::conection()->prepare("SELECT * FROM profesiones WHERE nombre = :nombre");

		$stmt -> bindParam(":nombre", $name , PDO::PARAM_STR);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}

}
?>
