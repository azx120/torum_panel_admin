<?php 

require_once ('conexion.php');

/*=============================================
MENSAJES
=============================================*/

class MdlMenssages
{
    /*=============================================
	MOSTRAR DISPUTAS SEGÚN PRODUCTO
	=============================================*/

	public static function mdlViewMenssage($table, $item, $value){

		if ($item != null) {

			$stmt = Conexion::conn()->prepare("SELECT * FROM $table WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $value, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();
			
		}else{

			$stmt = Conexion::conn()->prepare("SELECT * FROM $table");

			$stmt -> execute();

			return $stmt -> fetchAll();
			}

		$stmt-> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR MENSAJES POR LISTA
	=============================================*/

	public static function mdlViewMenssageList($table, $item, $value){

		if ($item != null) {

			$stmt = Conexion::conn()->prepare("SELECT * FROM $table WHERE $item = :$item ORDER BY id DESC");

			$stmt -> bindParam(":".$item, $value, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();
			
		}else{

			$stmt = Conexion::conn()->prepare("SELECT * FROM $table ORDER BY id DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();
			}

		$stmt-> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR UN SOLO CHAT 
	=============================================*/

	public static function mdlRequestChat($table, $dates){

		if ($dates !== "") {

			$stmt = Conexion::conn()->prepare("SELECT * FROM $table WHERE transmitter = :transmitter AND receiver = :receiver OR transmitter = :receiver AND receiver = :transmitter  ORDER BY id ASC");

			$stmt -> bindParam(":transmitter", $dates["transmite_request"], PDO::PARAM_STR);
			$stmt -> bindParam(":receiver", $dates["storeChat"], PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();
			
		}else {
			return "error";
		}
	}

	/*=============================================
	MOSTRAR DISPUTAS SEGÚN PRODUCTO
	=============================================*/

	public static function mdlViewMenssagePending($table, $item, $value, $status){

		if ($item != null) {

			$stmt = Conexion::conn()->prepare("SELECT * FROM $table WHERE $item = :$item AND status = $status");

			$stmt -> bindParam(":".$item, $value, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();
			
		}else{

			$stmt = Conexion::conn()->prepare("SELECT * FROM $table");

			$stmt -> execute();

			return $stmt -> fetchAll();
			}

		$stmt-> close();

		$stmt = null;

	}


	/*=============================================
	SING IN USER
	=============================================*/
	
	public static function mdlNewMessage($table, $d)
	{
		$stmt = Conexion::conn()->prepare("INSERT INTO $table (transmitter, receiver, message) VALUES (:transmitter, :receiver, :message)");

		$stmt->bindParam(":transmitter", $d["transmite"], PDO::PARAM_STR);
		$stmt->bindParam(":receiver", $d["recibe"], PDO::PARAM_STR);
		$stmt->bindParam(":message", $d["menssage"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;
	}

	/*=============================================
	MOSTRAR DISPUTAS SEGÚN PRODUCTO
	=============================================*/

	public static function mdlViewchat($table, $item, $value, $item2){

		if ($item != null) {

			$stmt = Conexion::conn()->prepare("SELECT * FROM $table WHERE $item = :$item OR $item2 = :$item ORDER BY id ASC");

			$stmt -> bindParam(":".$item, $value, PDO::PARAM_STR);
			$stmt -> bindParam(":".$item2, $value, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();
			
		}else{

			$stmt = Conexion::conn()->prepare("SELECT * FROM $table");

			$stmt -> execute();

			return $stmt -> fetchAll();
			}

		$stmt-> close();

		$stmt = null;

	}

}