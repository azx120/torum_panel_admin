<?php 
    $pagos = PagosModel::getAllPagos();
    $pagos_plus = PagosModel::getAllPagosPlus();
    $clientes = UserModel::getAllUsers();
    $getAllcursosHorarios = horarioModel::getAllcursosHorarios();
    $getAllcursosHorariosFtF = horarioFtfModel::getAllcursosHorariosFtF();
    $profesores = profesorController::allProfesor();
 
    
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Pagos de Clientes</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content mb-5">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h2 class="card-title">Pagos de instructores de Clientes</h2>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table id="dataTablePanel" class="table table-bordered table-striped">
              <thead>
                  <tr>
                      <th>
                          #
                      </th>
                      <th >
                          Cliente
                      </th>
                      <th >
                          Correo
                      </th>
                      <th >
                          Telefono
                      </th>
                      <th>
                         Tipo De Pago
                      </th>
                      <th>
                         Plan De Pago
                      </th>
                      <th  >
                          Comnpletado
                      </th>
                      <th>
                          Fecha De registro
                      </th>
                      
                      <th>
                          Estatus 
                      </th>
                  </tr>
              </thead>
              <tbody>
              <?php foreach($pagos as $pago):
                
                if($pago['status'] == 0){
                  $status = "Incompleto";
                  $status_button = "btn-success";
                  $status_class = "badge badge-danger";
                  $content_button = "Habilitar"; 
              }else{
                  $status = "Completo";
                  $status_button = "btn-danger";
                  $status_class = "badge badge-success";
                  $content_button = "Inabilitar";
              }

              ?>
              
                <tr>
                    <td>
                      #
                    </td>
                    
                      
                      <?php foreach($clientes as $cliente):
                      if($cliente["id"] == $pago["id_user"]): 
                        ?>
                        <td>
                          <p>
                            <?php echo $cliente['nombre'] . " " . $cliente['apellido']; ?>
                          </p>
                        </td>

                        <td>
                          <p>
                            <?php echo $cliente['mail']; ?>
                          </p>
                        </td>

                        <td>
                          <p>
                            <?php echo $cliente['telefono']; ?>
                          </p>
                        </td>
                      <?php 
                        endif;
                        endforeach; ?>
                                  
                    <td>
                      <p>
                        <?php echo $pago['type']; ?>
                      </p>
            
                    </td>
                    <td>
                    <?php 
                           if ($pago['type'] == "CURSO_GRUPAL"){

                            foreach($getAllcursosHorarios as $getAllcursosHorario):
                              if($getAllcursosHorario["id"] == $pago["value"]){
                                $data = $getAllcursosHorario['nombre'];
                             
                              foreach($profesores as $profesor):
                              if($profesor["id"] == $getAllcursosHorario["profesor_id"]){
                                $data_profesor = "Profesor: ". $profesor['nombre'] . " " .  $profesor['apellido'];
                              }
                              endforeach; 
                                  
                              }
                            endforeach;

                          }else if($pago['type'] == "CURSO_FTF"){

                            foreach($getAllcursosHorariosFtF as $getAllcursoHorarioFtF):
                              if($getAllcursoHorarioFtF["id"] == $pago["value"]){
                                $data = $getAllcursoHorarioFtF['nombre'];
                             
                              foreach($profesores as $profesor):
                              if($profesor["id"] == $getAllcursoHorarioFtF["profesor"]){
                                $data_profesor = "Profesor: ". $profesor['nombre'] . " " .  $profesor['apellido'];
                              }
                              endforeach;
                            }
                            endforeach;
                          }
                          ?>
                        <p>
                       
                            <?php echo $data; ?>
                        </p>
            
                    </td>
                    <td>
                        <p>
                            <?php
                              if($pago['completed'] == 1){
                                echo "pagado";
                              }else{
                                echo "No pagado";
                              }
                              
                             ?>
                        </p>
                    </td>
                    <td>
                        <p>
                          <?php echo $pago['created']; ?>
                        </p>
                    </td>
           
                    <td >
                        <span class="<?php echo $status_class; ?>"> <?php echo $status; ?></span>
                    </td>
                </tr>

            <?php endforeach; ?>
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card mb-5">
        <div class="card-header">
          <h2 class="card-title">Pagos de Cursos Plus de Clientes</h2>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table id="dataTablePanel2" class="table table-bordered table-striped">
              <thead>
                  <tr>
                      <th>
                          #
                      </th>
                      <th >
                          Cliente
                      </th>
                      <th >
                          Correo
                      </th>
                      <th >
                          Telefono
                      </th>
                      <th>
                         Plan De Pago
                      </th>
                      <th  >
                          Comnplatdo
                      </th>
                      <th>
                          Fecha De registro
                      </th>
                      
                      <th>
                          Estatus 
                      </th>
                  </tr>
              </thead>
              <tbody>
              <?php foreach($pagos_plus as $pago_plus):
                
                if($pago_plus['status'] == 0){
                  $status = "Incompleto";
                  $status_button = "btn-success";
                  $status_class = "badge badge-danger";
                  $content_button = "Habilitar"; 
              }else{
                  $status = "Completo";
                  $status_button = "btn-danger";
                  $status_class = "badge badge-success";
                  $content_button = "Inabilitar";
              }

              ?>
              
                <tr>
                    <td>
                      #
                    </td>
                    
                      
                      <?php foreach($clientes as $cliente):
                      if($cliente["id"] == $pago_plus["id_user"]): 
                        ?>
                        <td>
                          <p>
                            <?php echo $cliente['nombre'] . " " . $cliente['apellido']; ?>
                          </p>
                        </td>

                        <td>
                          <p>
                            <?php echo $cliente['mail']; ?>
                          </p>
                        </td>

                        <td>
                          <p>
                            <?php echo $cliente['telefono']; ?>
                          </p>
                        </td>
                      <?php 
                        endif;
                        endforeach; ?>
                                  
              
                    <td>
                    <?php 
                          if($pago_plus['type'] == "PLUS"){
                            $data = $pago_plus['plan'] . " meses";

                          }
                          ?>
                        <p>
                       
                            <?php echo $data; ?>
                        </p>
            
                    </td>
                    <td>
                        <p>
                            <?php
                              if($pago_plus['completed'] == 1){
                                echo "pagado";
                              }else{
                                echo "No pagado";
                              }
                              
                             ?>
                        </p>
                    </td>
                    <td>
                        <p>
                          <?php echo $pago_plus['created']; ?>
                        </p>
                    </td>
           
                    <td >
                        <span class="<?php echo $status_class; ?>"> <?php echo $status; ?></span>
                    </td>
                </tr>

            <?php endforeach; ?>
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->