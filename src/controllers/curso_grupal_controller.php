<?php 
class cursoGrupalController{

    public static function allCursosGrupal(){

        $prefesiones = new cursoGrupalModel();
        $all_prefesiones = $prefesiones -> getAllCursosGrupal();
        return $all_prefesiones;
    }

    public function getCursoGrupalById($post){

        $profesores = new cursoGrupalModel();
        $profesor_id = $profesores -> getCursoGrupalId($post);
        return $profesor_id;
    }

    public function editCursoGrupal($post){

        $profesor = new cursoGrupalModel();

        $getProfesor = $profesor -> getCursoGrupal($post);

        if ($getProfesor['id'] == $post['idCurso'] || empty($getProfesor)){

           $profesor_id = $profesor -> cursoGrupalEdit($post);

            if ($profesor_id == "ok"){
                echo "ok";
            }else {
                echo "error";
            }
        }else{
            echo "exist";
        } 
    }

    public function statusCursoGrupal($post){

        $profesores = new cursoGrupalModel();
        $all_profesores = $profesores -> statusCursoGrupal($post);
        return $all_profesores;
    }
     
    public function createCursoGrupal($post){

        $cursoNew = new cursoGrupalModel();
        $curso = $cursoNew -> getCursoGrupal($post);
        if ($curso == "" && $curso == null && $curso == false){
            $register = $cursoNew -> newCursoGrupal($post);
            if ($register == "ok"){
                echo "ok";
            }else {
                echo "error";
            }
        }else{
            echo "exist";
        }    
    } 

    public function addClientCursoGrupal($post){
        $users = new UserModel();
        $user = $users -> getUserMail($post);
        if (!empty($user)){
            $curso_gupal = new cursoGrupalModel();
           $getUser = $curso_gupal -> getClientCursoGrupal(["id" => $user["id"], "horario_id" => $post["id_horario"]]);
           if (empty($getUser)){
                $registerUser = $curso_gupal -> addClientHorario(["id" => $user["id"]],$post);
                return $registerUser;
           }else{
            echo "exist";
           }
        }else{
            echo "no_exist_user";
          }   
    }
 
}



?>
