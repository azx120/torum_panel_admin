<?php
 
 require_once '../controllers/upload.php';
 require_once '../controllers/profesor_controller.php';
require_once '../models/profesor_model.php';
require_once '../controllers/profesion_controller.php';
require_once '../models/profesion_model.php';



if (isset($_POST["registerprofesor"])){

    $profesor = profesorController::createProfesor($_POST);
    if( $profesor == "ok"){
        $array = ["mensaje" => "OK", "contenido" => "Profesor Registrado exitosamente!", "type" => "success"];
        echo json_encode($array);
    }else if ($profesor == "error"){
        $array = ["mensaje" => "ERROR", "contenido" => "Profesor no pudo ser Registrado!", "type" => "warning"];
        echo json_encode($array);

    }else if($profesor == "exist"){
        $array = ["mensaje" => "ERROR", "contenido" => "Profesor existente!", "type" => "warning"];
        echo json_encode($array);

    }
}
	 
if (isset($_POST["statusProf"])){

    $status_profesor = profesorController::statusProfesor($_POST);
   echo $status_profesor;
}

if (isset($_POST["getProfesorId"])){

    $profesor_by_id = profesorController::getProfesorById($_POST);
    
   echo json_encode($profesor_by_id);
}

if (isset($_POST["editProfesor"])){

    $edit_profesor = profesorController::editProfesor($_POST);

    if( $edit_profesor == "ok"){
        $array = ["mensaje" => "OK", "contenido" => "Profesor Editado exitosamente!", "type" => "success"];
        echo json_encode($array);
    }else if ($edit_profesor == "error"){
        $array = ["mensaje" => "ERROR", "contenido" => "Profesor no pudo ser Editado!", "type" => "warning"];
        echo json_encode($array);

    }else if($edit_profesor == "exist"){
        $array = ["mensaje" => "ERROR", "contenido" => "Profesor existente!", "type" => "warning"];
        echo json_encode($array);

    }

}
?>