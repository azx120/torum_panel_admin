<?php 
    $clientes = UserModel::getAllUsers();
    $cursos = cursoModel::getAllCursos_plus();
    $all_cursando_plus = cursoModel::getAllCursando_plus();

  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Cursando Plus Clientes</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo $urls; ?>">Home</a></li>
              <li class="breadcrumb-item active">Projects</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Projects</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table id="dataTablePanel" class="table table-bordered table-striped">
              <thead>
                  <tr>
                      <th>
                          #
                      </th>
                      <th >
                          Cliente
                      </th>
                      <th>
                         Curso
                      </th>
                      <th>
                         Plan
                      </th>
                      <th>
                        Registro
                      </th>
                      <th>
                          Estatus 
                      </th>
                      <th >
                        Acciones
                      </th>
                  </tr>
              </thead>
              <tbody>
              <?php foreach($all_cursando_plus as $cursando_plus):
                foreach($cursos as $curso): 

                 if($cursando_plus["curso_id"] == $curso["id"]):

                if($cursando_plus['status'] == 0){
                  $status = "Inactivo";
                  $status_button = "btn-success";
                  $status_class = "badge badge-danger";
                  $content_button = "Habilitar"; 
              }else{
                  $status = "Activo";
                  $status_button = "btn-danger";
                  $status_class = "badge badge-success";
                  $content_button = "Inabilitar";
              }
              ?>
              
                <tr>
                    <td>
                     
                    </td>
                    <td>
                      <p>
                        <?php foreach($clientes as $cliente): 
                            if($cursando_plus["user_id"] == $cliente["id"]):
                            
                                echo $cliente['nombre'];
                            endif;
                        endforeach; 
                        ?>
                      </p>
                        
                    </td>
                    <td>
                      <p>
                        <?php echo $curso['nombre'];  ?>
                      </p>
            
                    </td>
                    <td>
                        <p>
                            <?php echo $cursando_plus['plan']. " MESES"; ?>
                        </p>
            
                    </td>
                    <td>
                        <p>
                          <?php echo $cursando_plus['created']; ?>
                        </p>
                    </td>
  
                    <td >
                        <span id="cursandoStatusPlus_<?php echo $cursando_plus['id']; ?>" class="<?php echo $status_class; ?>"> <?php echo $status; ?></span>
                    </td>
                    <td class="project-actions">
                    
                        <button class="btn <?php echo $status_button ?> btn-sm statusCursandoPlus" id="statusCursandoPlus_<?php echo $cursando_plus['id']; ?>" value="<?php echo $cursando_plus['id']; ?>">
                            <i class="fas fa-trash">
                            </i>
                            <?php echo $content_button ?> 
                        </button>
                    </td>
                </tr>

            <?php 
                    endif;
                endforeach;    
            endforeach; ?>
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
      <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
                            <i class="fas fa-pencil-alt">
                            </i>
                           Asignar Curso a Cliente
                        </button>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Datos del Admininstrador</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post">
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="" id="emailClient" placeholder="Email">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <select class="form-control" name="planPlusCursando" id="planPlusCursando" placeholder="Plan Curso">
              <option value="">Selecione si es o no Plus</opton>
              <option value="1">1 Mes</opton>
              <option value="3">3 Meses</opton> 
              <option value="6">6 Meses</opton>              
              <option value="12">12 Meses</opton> 
            </select> 
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>

          <div class="input-group mb-3">
            <select class="form-control" name="cursoCursando" id="cursoCursando" >
              <option value="">Curso</opton>
              <?php foreach($cursos as $curso): ?>
                <option value="<?php echo $curso['id'] ?>"><?php echo $curso['nombre'] ?></opton>
              <?php endforeach; ?>
            </select> 
          </div>
        
          
          <div class="row">
          </div>
        </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="registerCursandoPlus">Save changes</button>
      </div>
    </div>
  </div>
</div>
