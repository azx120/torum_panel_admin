<?php

require_once '../controllers/profesor_controller.php';
require_once '../models/profesor_model.php';
require_once '../controllers/profesion_controller.php';
require_once '../models/profesion_model.php';


class TablaProfesor{

    /*=============================================
    MOSTRAR LA TABLA DE SUBCATEGORÍAS
    =============================================*/ 
  
    public function mostrarTablaProfesores(){	
  
  
        $profesores = profesorController::allProfesor();
        $profesiones = profesionController::allPrefesion();
  
        $datosJson = '{
  
        "data": [ ';
  
          for($i = 0; $i < count($profesores); $i++){

            if($profesores['status'] == 0){
                $status = "Inactivo";
                $status_button = "btn-success";
                $status_class = "badge badge-danger";
                $content_button = "Habilitar"; 
            }else{
                $status = "Activo";
                $status_button = "btn-danger";
                $status_class = "badge badge-success";
                $content_button = "Inabilitar";
            }
  
           foreach($profesiones as $profesion):
                if($profesor['profesion_id'] == $profesion['id'] ){
                    $profesion_prof = $profesion['nombre'];  
                }
            endforeach;
  
  
                $estado = "<span id='profStatus_".$profesores['id']."' class='".$status_class."'>".$status."</span>";
  
  
  
                /*=============================================
                CREAR LAS ACCIONES
                =============================================*/
  
                $acciones = "
                <div class='btn-group'>
                    <button class='btn btn-info btn-sm getProf' data-toggle='modal' data-target='#exampleModalCenter' value='".$profesores['id']."'>
                        <i class='fas fa-pencil-alt'>
                        </i>
                        Editar
                    </button>
                    <button class='btn ".$status_button." btn-sm statusProf' id='statusProf_". $profesores['id']." ' value='".$profesores['id']."'>
                        <i class='fas fa-trash'>
                        </i>
                        ".$content_button." 
                    </button>
                </div>";
  
  
               $datosJson .=  '
               [
                "'.($i+1).'",
                "'.$profesores['nombre'].'",
                "'.$profesores['apellido'].'",
                "'.$profesores['mail'].'",
                "'.$profesion_prof.'",
                "'.$estado.'",
                "'.$acciones.'"
              ],';
                                      
              }
  
              $datosJson =  substr($datosJson, 0, -1);
              $datosJson .=  '
              
            ]
          }';
  
      echo $datosJson;    
        
    }
  
  }
  
  /*=============================================
  ACTIVAR TABLA DE SUBCATEGORÍAS
  =============================================*/ 
  $activarSubcategoria = new TablaProfesor();
  $activarSubcategoria -> mostrarTablaProfesores();
  
  
?>