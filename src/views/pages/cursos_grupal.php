<?php 
    $cursos = cursoGrupalController::allCursosGrupal();
    $profesiones = profesionController::allPrefesion();
     
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Cursos De Instructores</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo $urls; ?>">Home</a></li>
              <li class="breadcrumb-item active">Projects</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Projects</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table id="dataTablePanel" class="table table-bordered table-striped">
              <thead>
                  <tr>
                      <th >
                          #
                      </th>
                      <th >
                          Nombre
                      </th>
                      <th  >
                          Prfesión
                      </th>
                      <th>
                          Status
                      </th>
                      <th style="width: 20%">
                        Acciones
                      </th>
                  </tr>
              </thead>
              <tbody>
              <?php foreach($cursos as $curso):
                
                if($curso['status'] == 0){
                  $status = "Inactivo";
                  $status_button = "btn-success";
                  $status_class = "badge badge-danger";
                  $content_button = "Habilitar"; 
              }else{
                  $status = "Activo";
                  $status_button = "btn-danger";
                  $status_class = "badge badge-success";
                  $content_button = "Inabilitar";
              }
             
              ?>
              
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        <p>
                            <?php echo $curso['nombre']; ?>
                        </p>
                    </td>
                        
                    <td class="project_progress">

                    <?php foreach($profesiones as $profesion):
                            if($curso['profesion_id'] == $profesion['id'] ){
                                echo $profesion['nombre'];  
                            }
                        endforeach;
                    ?>

                    </td>
                    <td >
                        <span id="cursoStatus_<?php echo $curso['id']; ?>" class="<?php echo $status_class; ?>"> <?php echo $status; ?></span>
                    </td>
                    <td class="project-actions">
                    
                        <button class="btn btn-info btn-sm getCursoGrupal" data-toggle="modal" data-target="#exampleModalCenter" value="<?php echo $curso['id']; ?>">
                            <i class="fas fa-pencil-alt">
                            </i>
                            Editar
                        </button>
                        <button class="btn <?php echo $status_button ?> btn-sm statusCursoGrupal" id="statusCurso_<?php echo $curso['id']; ?>" value="<?php echo $curso['id']; ?>">
                            <i class="fas fa-trash">
                            </i>
                            <?php echo $content_button ?> 
                        </button>
                    </td>
                </tr>

            <?php endforeach; ?>
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Datos del Admininstrador</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post">
      <input type="hidden" class="form-control" name="" id="idCurso" placeholder="Nombre Del Curso">
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="" id="nameCurso" placeholder="Nombre Del Curso">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <textarea class="form-control" name="" id="botonFtf" placeholder="Boton de pago Face To  Face"></textarea>
        
          </div>
          <div class="input-group mb-3">
            <select class="form-control" name="rolAdmin" id="profesionCurso" placeholder="Role">
              <?php foreach($profesiones as $profesion): ?>
                <option value="<?php echo $profesion['id'] ?>"><?php echo $profesion['nombre'] ?></opton>
              <?php endforeach; ?>
            </select> 
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
        </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="editCursoGrupal">Save changes</button>
      </div>
    </div>
  </div>
</div>