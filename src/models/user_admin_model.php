<?php
require_once "conexion.php";

class UserAdminModel{

	//crear nuevo usuario
	public function getAllAdmin(){

		$stmt = Conexion::conection()->prepare("SELECT * FROM user_admin");

		$stmt->execute();
		return $stmt->fetchall();

	}

	public function statusAdmin($post){
		
		$stmt = Conexion::conection()->prepare("UPDATE user_admin SET status = NOT status WHERE id = :id");
		$stmt->bindParam(":id", $post["status"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

	}

	public function adminEdit($post){

		$pass = crypt($post['passAdmin'], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
		
		$stmt = Conexion::conection()->prepare("UPDATE user_admin SET username = :username, name = :name, mail = :mail, password = :password, rol = :rol WHERE id = :id");
		
		$stmt->bindParam(":id", $post["idAdmin"], PDO::PARAM_INT);
		$stmt->bindParam(":username", $post["nameUserADmin"], PDO::PARAM_STR);
		$stmt->bindParam(":name", $post["nameAdmin"], PDO::PARAM_STR);
		$stmt->bindParam(":mail", $post["mailAdmin"], PDO::PARAM_STR);
		$stmt->bindParam(":password", $pass, PDO::PARAM_STR);
		$stmt->bindParam(":rol", $post["rolAdmin"], PDO::PARAM_STR);
		
	
			
		if($stmt -> execute()){

			return "ok";	
		
		}else{

			return "error";	

		}

	}
	
	//crear nuevo usuario
	public function newAdminUser($post){

		$pass = crypt($post['passAdmin'], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
		$stmt = Conexion::conection()->prepare("INSERT INTO user_admin 
		(username, name, mail, password, rol, status)VALUES(:username, :name, :mail, :password, :rol, 1)");

		$stmt->bindParam(":username", $post["nameUserADmin"], PDO::PARAM_STR);
		$stmt->bindParam(":name", $post["nameAdmin"], PDO::PARAM_STR);
		$stmt->bindParam(":mail", $post["mailAdmin"], PDO::PARAM_STR);
		$stmt->bindParam(":password", $pass, PDO::PARAM_STR);
		$stmt->bindParam(":rol", $post["rolAdmin"], PDO::PARAM_STR);

		if($stmt->execute()){
			return "ok";
		}else{
			return "error";
		}
	}
	
	//Metodos de consulta
	public function getUser($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM user_admin WHERE username = :username OR mail = :mail");

		$stmt -> bindParam(":username", $post['nameUserADmin'], PDO::PARAM_STR);
		$stmt -> bindParam(":mail", $post['mailAdmin'], PDO::PARAM_STR);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}

	
	//Metodos de consulta
	public function getAdminId($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM user_admin WHERE id = :id");

		$stmt -> bindParam(":id", $post['id'], PDO::PARAM_STR);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}

    //Metodos de consulta
    public function getLoginUser($post){

		$pass = crypt($post['password'], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
        $stmt = Conexion::conection()->prepare("SELECT * FROM user_admin WHERE username = :username AND password = :password OR mail = :mail AND password = :password");

		$stmt -> bindParam(":username", $post['usernameLogin'], PDO::PARAM_STR);
		$stmt -> bindParam(":password", $pass, PDO::PARAM_STR);
		$stmt -> bindParam(":mail", $post['usernameLogin'], PDO::PARAM_STR);


		$stmt ->execute();
      
		return $stmt ->fetch();
		

		$stmt = null;

    }
}
?>
