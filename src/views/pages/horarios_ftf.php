<?php 
    $horarios = horarioFtfController::allHorarios();
    $cursos = cursoGrupalController::allCursosGrupal();
    $profesiones = profesionController::allPrefesion();
    $profesores = profesorController::allProfesor();
    $dias = array("domingo","lunes","martes","miércoles","jueves","viernes","sábado");
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Horarios Face to Face Para Instructores</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo $urls; ?>">Home</a></li>
              <li class="breadcrumb-item active">Projects</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Projects</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table id="dataTablePanelNot" class="table table-bordered table-striped">
              <thead>
                  <tr> 
                      <th >
                          #
                      </th>
                      <th >
                          profesor
                      </th>
                      <th>
                          Dias Semanas
                      </th>
                      <th>
                          hora inicio
                      </th>
                      <th>
                          hora finalizar
                      </th>
                      <th >
                          Nro Max Cleintes
                      </th>
                      <th>
                         curso
                      </th>
                      <th>
                         Link Zoom
                      </th>
                      <th>
                          Status
                      </th>
                      <th style="width: 20%">
                        Acciones
                      </th>
                  </tr>
              </thead>
              <tbody>
              <?php foreach($horarios as $horario):
                
                if($horario['status'] == 0){
                  $status = "Inactivo";
                  $status_button = "btn-success";
                  $status_class = "badge badge-danger";
                  $content_button = "Habilitar"; 
              }else{
                  $status = "Activo";
                  $status_button = "btn-danger";
                  $status_class = "badge badge-success";
                  $content_button = "Inabilitar";
              }
              /*if($curso['plus'] == 0){
                $plus = "no";
               }else{
                $plus = "si";
               }*/
              ?>
              
                <tr>
                    <td>
                    <a class="btn btn-info btn-sm getHorario" href="<?php echo $urls."?urls=listaCursoFtF&id_horario=".$horario['id']; ?>">
                          <i class="fas fa-pencil-alt">
                          </i>
                          Ver Lista de Alumnos
                      </a>
                    </td>
                    <td>
                      <?php foreach($profesores as $profesor):
                          if($horario['profesor_id'] == $profesor['id'] ){
                              echo "<p>".$profesor['nombre']."</p>";  
                          }
                      endforeach;?>
                    </td>
                    <td>
                      <p>
                            <?php echo $horario['numero_cleintes']; ?>
                        </p>
                    </td>
                    <td>
                
                        <?php foreach($cursos as $curso):
                            if($horario['curso_id'] == $curso['id'] ){
                                echo "<p>".$curso['nombre']."</p>";  
                            }
                        endforeach;
                        ?>
            
                    </td>
                    <td>
                        <p>
                            <?php echo $horario['link_zoom']; ?>
                        </p>
            
                    </td>
                    <td>
                        <p>
                            <?php echo $horario['diasemana']; ?>
                        </p>
            
                    </td>
                    <td>
                        <p>
                            <?php echo $horario['hora_inicio']; ?>
                        </p>
            
                    </td>
                    <td>
                        <p>
                            <?php echo $horario['hora_finalizar']; ?>
                        </p>
            
                    </td>
                    <td >
                        <span id="horarioFtfStatus_<?php echo $horario['id']; ?>" class="<?php echo $status_class; ?>"> <?php echo $status; ?></span>
                    </td>
                    <td class="project-actions">
                    
                        <button class="btn btn-info btn-sm getHorarioFtf" data-toggle="modal" data-target="#exampleModalCenter" value="<?php echo $horario['id']; ?>">
                            <i class="fas fa-pencil-alt">
                            </i>
                            Editar
                        </button>
                        <button class="btn <?php echo $status_button ?> btn-sm statusHorarioFtf" id="statusHorarioFtf_<?php echo $horario['id']; ?>" value="<?php echo $horario['id']; ?>">
                            <i class="fas fa-trash">
                            </i>
                            <?php echo $content_button ?> 
                        </button>
                    </td>
                </tr>

            <?php endforeach; ?>
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Datos del Admininstrador</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post">
          <input class="form-control" type="hidden" id="id_horaio">
          <div class="input-group mb-3">
            <input class="form-control" type="time" id="hora_inicio">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <label >hora de finalizar</label>
          <div class="input-group mb-3">
            <input class="form-control" type="time" id="hora_finalizar">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="" id="diasemana" placeholder="Dias de la semana (ejemplo, ejemplo, etc)">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div> 
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="" id="linkZoom" placeholder="Link de Zoom">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input class="form-control" type="number" id="nro_client" placeholder="Nro de clientes">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <select class="form-control" name="rolAdmin" id="profesion_id" >
              <option value="">Profesion</opton>
              <?php foreach($profesiones as $profesion): ?>
                <option value="<?php echo $profesion['id'] ?>"><?php echo $profesion['nombre'] ?></opton>
              <?php endforeach; ?>
            </select> 
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <select class="form-control" name="rolAdmin" id="curso_id" placeholder="Role"  disabled>
              
            </select> 
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <select class="form-control" name="rolAdmin" id="profesor_id" placeholder="Role" disabled>
              
            </select> 
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          
          <div class="row">
          </div>
        </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="editHoraio_ftf">Save changes</button>
      </div>
    </div>
  </div>
</div>