<?php 
require_once "conexion.php";

class cursoGrupalModel{

	//crear nuevo usuario
	public function getAllCursosGrupal(){

		$stmt = Conexion::conection()->prepare("SELECT * FROM cursos_grupal");

		$stmt->execute();
		return $stmt->fetchall();

	}

	public function statusCursoGrupal($post){

		$stmt = Conexion::conection()->prepare("UPDATE cursos_grupal SET status = NOT status WHERE id = :id");
		$stmt->bindParam(":id", $post["status"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

	}

	public function statusCursoGrupalCliente($post){
		$stmt = Conexion::conection()->prepare("UPDATE cursando_grupal SET status = NOT status WHERE id = :id");
		$stmt->bindParam(":id", $post["status"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

	}

	public function cursoGrupalEdit($post){
		

		$stmt = Conexion::conection()->prepare("UPDATE cursos_grupal SET nombre = :nombre, profesion_id = :profesionId, boton_ftf = :boton_ftf WHERE id = :id");
		
		$stmt->bindParam(":id", $post["idCurso"], PDO::PARAM_INT);
		$stmt->bindParam(":nombre", $post["nameCurso"], PDO::PARAM_STR);
		$stmt->bindParam(":profesionId", $post["profesionCurso"], PDO::PARAM_STR);
		$stmt->bindParam(":boton_ftf", $post["botonFtf"], PDO::PARAM_STR);
	
	
			
		if($stmt -> execute()){

			return "ok";	
		
		}else{

			return "error";	

		}

	}
	
	//crear nuevo curso
	public function newCursoGrupal($post){

		$stmt = Conexion::conection()->prepare("INSERT INTO cursos_grupal (nombre,  profesion_id, boton_ftf, status) VALUES (:nombre, :profesionId,:boton_ftf, 1)");


		$stmt->bindParam(":nombre", $post["nameCurso"], PDO::PARAM_STR);
		$stmt->bindParam(":profesionId", $post["profesionCurso"], PDO::PARAM_STR);
		$stmt->bindParam(":boton_ftf", $post["botonFtf"], PDO::PARAM_STR);
	

		if($stmt->execute()){
			return "ok";
		}else{
			return "error";
		}
	}
	
	
	//Metodos de consulta
	public static function getCursoGrupalId($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM cursos_grupal WHERE id = :id");

		$stmt -> bindParam(":id", $post['id'], PDO::PARAM_INT);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}
	
		//Metodos de consulta
		public function getCursoGrupalByProfesion($post){

			$stmt = Conexion::conection()->prepare("SELECT * FROM cursos_grupal WHERE profesion_id = :profesion_id AND status = 1");
	
			$stmt -> bindParam(":profesion_id", $post['profesion_id'], PDO::PARAM_INT);
	
			$stmt ->execute();
			
			return $stmt ->fetchAll();
	
			$stmt = null;
	
		}
    
    //Metodos de consulta
	public function getCursoGrupal($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM cursos_grupal WHERE nombre = :nombre");

		$stmt -> bindParam(":nombre", $post['nameCurso'] , PDO::PARAM_STR);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}

	  //Metodos de consulta
	  public function getClientCursoGrupal($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM cursando_grupal WHERE user_id = :id AND horario_id = :horario_id");

		$stmt -> bindParam(":id", $post['id'] , PDO::PARAM_INT);
		$stmt -> bindParam(":horario_id", $post['horario_id'] , PDO::PARAM_INT);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}

	//Metodos de consulta
	public static function addClientHorario($id, $post){

		$fecha = date("Y-m-d");
		$stmt = Conexion::conection()->prepare("INSERT INTO cursando_grupal (user_id, horario_id,status,created) VALUES (:user_id, :horario_id, 1,:created)");

		$stmt -> bindParam(":user_id", $id['id'], PDO::PARAM_INT);
		$stmt -> bindParam(":horario_id", $post['id_horario'], PDO::PARAM_INT);
		$stmt -> bindParam(":created", $fecha, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}
	}

}
?>
