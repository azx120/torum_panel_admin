<?php

Class ControladorNotificaciones{

	/*=============================================
	MOSTRAR NOTIFICACIONES
	=============================================*/

	static public function ctrMostrarNotificaciones(){

		$tabla = "tc_notification";

		$respuesta = ModeloNotificaciones::mdlMostrarNotificaciones($tabla);

		return $respuesta;

	}

	/*=============================================
	MOSTRAR NOTIFICACIONES Vendedor
	=============================================*/

	static public function ctrMostrarNotificacionesVendedor($id){

		$tabla = "tc_notification_vendor";

		$respuesta = ModeloNotificaciones::mdlMostrarNotificacionesVendedor($tabla,$id);
		
		return $respuesta;

	}

	/*=============================================
	crear NOTIFICACIONES
	=============================================*/

	static public function ctrCrearNotificaciones($id_user, $id_ref, $type){

		$tabla = "tc_notification";

		$respuesta = ModeloNotificaciones::mdlCrearNotificaciones($tabla,$id_user, $id_ref, $type);

		return $respuesta;

	}
	/*=============================================
	MOSTRAR NOTIFICACIONES DE USUARIOS
	=============================================*/



}