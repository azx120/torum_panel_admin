<?php

require_once "conexion.php";

class ModeloNotificaciones{
		
	/*=============================================
	MOSTRAR NOTIFICACIONES
	=============================================*/

	static public function mdlMostrarNotificaciones($tabla){
		
		$stmt = Conexion::conn()->prepare("SELECT * FROM $tabla ORDER BY id DESC");


		$stmt -> execute();

		return $stmt -> fetchall();

		$stmt -> close();
		
		$stmt = null;
	 
	}
	
	/*=============================================
	MOSTRAR NOTIFICACIONES
	=============================================*/

	static public function mdlMostrarNotificacionesVendedor($tabla, $id){
		
		$stmt = Conexion::conn()->prepare("SELECT * FROM $tabla WHERE id_user = :iduser AND status_check = 0  AND account = 'SELLER' ORDER BY id DESC");

		$stmt -> bindParam(":iduser", $id, PDO::PARAM_INT);

		$stmt -> execute();

		$notifications = $stmt -> fetchall();
		
		$saleCount = 0;
		$visitorCount = 0;
		$messageCount = 0;
		$nUserCount = 0;

		foreach ($notifications as $value){
			if($value["type"] == "SALE"){
				$saleCount++; 
			}else if ($value["type"] == "NEW_VISITOR"){
				$visitorCount++;
			}else if ($value["type"] == "NEW_USER"){
				$nUserCount++;
			}else if ($value["type"] == "MESSAGE"){
				$messageCount++;
			}
		}
		return $allNotifications = ["all" => $notifications, "SALE" => $saleCount, "NEW_VISITOR"=>$visitorCount,"NEW_USER"=>$nUserCount, "MESSAGE"=> $messageCount];
		$stmt -> close();
		
		$stmt = null;
	 
	}

	/*=============================================
	ACTUALIZAR NOTIFICACIONES
	=============================================*/

	static public function mdlActualizarNotificaciones($tabla, $id){

		$stmt = Conexion::conn()->prepare("UPDATE $tabla SET status_check = 1 WHERE :id = $id");

		$stmt -> bindParam(":id", $id, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}
 /*=============================================
	CREAR NOTIFICACIONES
	=============================================*/

	static public function mdlCrearNotificaciones($tabla,$id_user, $id_ref, $type){

		$fecha = date();
		$message = "menssage de prueba";

		$stmt = Conexion::conn()->prepare("INSERT INTO $tabla ('id_user', 'type', 'id_ref', 'message', 'date', 'status_check' ) 
		VALUES ($id_user, $type, $id_ref, $message, $fecha, 0");


		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;

		

	}

	
}