<?php 

class CTR_template
{
	/*=============================================
	INICIAMOS EL TEMPLATE PRINCIPAL
	=============================================*/

	public function template()
	{
		require_once( APPPATH.'views/template.php');
	}

	/*=============================================
	TRAEMOS LOS ESTILOS DINÁMICOS DE LA PLANTILLA
	=============================================*/

	public function ctrStyleTtemplate(){

		$table = "tc_template";

		$response = ModelsTemplate::mdlLStyleTemplate($table);

		return $response;
	}


	/*=============================================
	GUARDAR DATOS NEWSLETTER
	=============================================*/

	public static function ctrInsertNewsletter($dates){

		$table = "tc_newsletter";

		$response = ModelsTemplate::mdlInsertNewsletter($table, $dates);

		return $response;
	}


	/*=============================================
	VER DATOS DEL NEWSLETTER
	=============================================*/

	public static function ctrViewNewslleter($item, $value){

		$table = "tc_newsletter";

		$response = ModelsTemplate::mdlViewNewslleter($table, $item, $value);

		return $response;
	}


}
