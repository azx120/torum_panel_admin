<div class="login-page">  
  <div class="register-box">
    
      <h2 align="center"><b>Registro</b> </br>Nueva Profesion</h2>
    

    <div class="card">
      <div class="card-body register-card-body">
        <p class="login-box-msg">Rellenar todos los datos</p>

        <form method="post">
          <div class="input-group mb-3">
            <input type="text" class="form-control"  id="nombreProfesion" placeholder="Nombre De La Profesión">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control" id="ramaProfesion" placeholder="Rama De esta profesión">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          
          <div class="row">
       
            <!-- /.col -->
            <div class="col-12">
              <button type="submit" class="btn btn-primary btn-block" id="nuevaProfesion">Register</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>
      <!-- /.form-box -->
    </div><!-- /.card -->
  </div>
<!-- /.register-box -->
</div>
