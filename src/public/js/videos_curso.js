$( document ).ready(function() {

    $(".selectVideo").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();
        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/videoUp_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response !== "") {
                    var obj = jQuery.parseJSON(response);
                    $("#idVideo").val(obj.id);
                    $("#nameVideo").val(obj.nombre);
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("getVideoId=true&id="+ id);
    })

    $("#sendVideoCurso").on('click',  function(event) {
        event.preventDefault();
        
        var register_video_curso = $("#register_video_curso").val();
        var longitudVideosCurso = parseInt($("#nroVideosCurso").attr("max"));

        var titleVideoCurso = $("#titleVideoCurso").val();
        var nroVideosCurso = $("#nroVideosCurso").val();
        var idVideo = $("#idVideo").val();
        var id_curso = $("#id_curso").val();
        var id_video_curso = $("#id_video_curso").val();
        
      
        var url = $("#url").val();

        if (titleVideoCurso =='') {
            swal('Error', 'Error titulo vacio!', 'error');
        
            return false;
        };
     
        if (nroVideosCurso =='') {
            swal('Error', 'Error numero del video vacio!', 'error');
        
            return false;
        }else if (nroVideosCurso > longitudVideosCurso || nroVideosCurso < 1){
            swal('Error', 'Error numero para el video incorrecto!', 'error');
            return false;
        };

        if (idVideo =='') {
            swal('Error', 'no ha selecionado un video para el curso!', 'error');
        
            return false;
        };
    

        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/video_curso_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    swal({
                        title: "¡OK!",
                        text: "Operación realizada exitosamente!",
                        type: "success",
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    },
                    
                    function(isConfirm) {
                        if (isConfirm) {
                            window.location.reload();
                    
                        }
                    });
                }else if (response == "exist") {

					swal('Error', 'Error ya este curso esta registrado!', 'error');

				};
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("register_video_curso="+register_video_curso+"&titleVideoCurso="+ titleVideoCurso +"&nroVideosCurso="+nroVideosCurso +"&idVideo="+ idVideo + "&id_curso="+ id_curso+"&id_video_curso="+id_video_curso);
    

    })

    $(".statusVideoCurso").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();

       var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/video_curso_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    var st = $("#videoCursoStatus_"+id).attr("class");
                    if(st == "badge badge-danger"){
                      
                        $("#videoCursoStatus_"+id).attr("class","badge badge-success");
                        $("#videoCursoStatus_"+id).text("Activo");
                        
                     
                        $("#statusVideoCurso_"+id).addClass("btn-danger");
                        $("#statusVideoCurso_"+id).removeClass("btn-success");
                        $("#statusVideoCurso_"+id).text("Inhabilitar ");
                 

                    }else if(st == "badge badge-success"){

                        $("#videoCursoStatus_"+id).attr("class","badge badge-danger");
                        $("#videoCursoStatus_"+id).text("Inactivo");
                 

                        $("#statusVideoCurso_"+id).addClass("btn-success");
                        $("#statusVideoCurso_"+id).removeClass("btn-danger");
                        $("#statusVideoCurso_"+id).text("Habilitar");
                        

                    }
                    console.log(st);
                }else if(response == "error"){
                    console.log("erro");
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("statusVideoCurso=true&status="+ id);

    })

    $(".getVideoCurso").on('click',  function(event) {
        var id = $(this).val();
        var nroVideo = $("#nroVideo_"+id).text();
        var tituloVideo = $("#tituloVideo_"+id).attr("value");
        var videoId = $("#video_id_"+id).text();
        var nombreVideo = $("#nombreVideo_"+id).attr("value");
        var id_video_curso = $("#id_video_"+id).attr("value");

        $("#nroVideosCurso").val(parseInt(nroVideo));
        $("#titleVideoCurso").val(tituloVideo);
        $("#nameVideo").val(nombreVideo); 
        $("#idVideo").val(videoId);
        $("#sendVideoCurso").text("Editar");
        $("#mensaje_form").text('El formulario esta en modo editar, para volver al modo Registrar precione el boton "vaciar"');
        $("#register_video_curso").val("false");
        $("#id_video_curso").val(id_video_curso);

    })

        
});
