
  <?php 
    $profesores = profesorController::allProfesor();
    $profesiones = profesionController::allPrefesion();
    
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Instructores</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo $urls; ?>">Home</a></li>
              <li class="breadcrumb-item active">Projects</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Projects</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table id="dataTablePanel" class="table table-bordered table-striped">
              <thead>
                  <tr>
                      <th>
                          #
                      </th>
                      <th >
                          Nombre
                      </th>
                      <th >
                          Apellido
                      </th>
                      <th>
                         Correo
                      </th>
                      <th  >
                          Prfesión
                      </th>
                      <th  >
                          Descripcion
                      </th>
                      <th>
                          Status
                      </th>
                      <th style="width: 20%">
                        Acciones
                      </th>
                  </tr>
              </thead>
              <tbody>
              <?php foreach($profesores as $profesor):
                
                if($profesor['status'] == 0){
                  $status = "Inactivo";
                  $status_button = "btn-success";
                  $status_class = "badge badge-danger";
                  $content_button = "Habilitar"; 
              }else{
                  $status = "Activo";
                  $status_button = "btn-danger";
                  $status_class = "badge badge-success";
                  $content_button = "Inabilitar";
              }
              ?>
              
                <tr>
                    <td>
                      <img src="<?php echo $UrlPortal; ?>src/public/img/instructores/<?php echo $profesor['img']; ?>" width="100px">
                    </td>
                    <td>
                        <p>
                            <?php echo $profesor['nombre']; ?>
                        </p>
                        <!--<br/>
                        <small>
                            Created 01.01.2019
                        </small>-->
                    </td>
                    <td>
                        <p>
                            <?php echo $profesor['apellido']; ?>
                        </p>
            
                    </td>
                    <td class="project_progress">
                    <?php echo $profesor['mail']; ?>
                    </td>
                    <td class="project_progress">

                    <?php foreach($profesiones as $profesion):
                            if($profesor['profesion_id'] == $profesion['id'] ){
                                echo $profesion['nombre'];  
                            }
                        endforeach;
                    ?>

                    </td>
                    <td>
                        <p>
                            <?php echo $profesor['descripcion']; ?>
                        </p>
            
                    </td>
                    <td >
                        <span id="profStatus_<?php echo $profesor['id']; ?>" class="<?php echo $status_class; ?>"> <?php echo $status; ?></span>
                    </td>
                    <td class="project-actions"> 
                    
                        <button class="btn btn-info btn-sm getProf" data-toggle="modal" data-target="#exampleModalCenter" value="<?php echo $profesor['id']; ?>">
                            <i class="fas fa-pencil-alt">
                            </i>
                            Editar
                        </button>
                        <button class="btn <?php echo $status_button ?> btn-sm statusProf" id="statusProf_<?php echo $profesor['id']; ?>" value="<?php echo $profesor['id']; ?>">
                            <i class="fas fa-trash">
                            </i>
                            <?php echo $content_button ?> 
                        </button>
                    </td>
                </tr>

            <?php endforeach; ?>
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Datos del Admininstrador</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

          <form method="post" id="form_teacher">
          <div class="input-group mb-3">
          <input type="hidden" class="" name="nameProfesor2" id="">
          <input type="hidden" class="" name="editProfesor" id="" value="true">
          <input type="hidden" class="" name="idProfesor" id="idProfesor">
            <input type="text" class="form-control" name="nameProfesor" id="nameProfesor" placeholder="Nombre Completo">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="apellidoProfesor" id="apellidoProfesor" placeholder="Apellido Completo">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="email" class="form-control" name="mailProfesor" id="mailProfesor" placeholder="Correo Electronico">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="passProfesor" id="passProfesor" placeholder="Contraseña">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="file" class="form-control-file" name="imgPerson" id="imgPerson" placeholder="Foto">
            <input type="hidden" class="form-control-file" name="imgPersonHidden" id="imgPersonHidden">
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="especialidad" id="especialidad" placeholder="Especialidad Del Profesor">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-graduation-cap"></span>
              </div>
            </div>
          </div>
          <label>Descripcion del Instructor</label>
          <div class="input-group mb-3">
          <textarea class="editor1" name="editor1" id="editor"></textarea>
  
          </div>
          <div class="input-group mb-3">
          
            <textarea class="form-control" name="botonFtf" id="botonFtf" placeholder="Boton de pago Face To  Face"></textarea>
        
          </div> 
          <div class="input-group mb-3">
            <select class="form-control" name="profesionProfesor" id="profesionProfesor" placeholder="Role">
              <option value="">Profesion</opton>
              <?php foreach($profesiones as $profesion): ?>
                <option value="<?php echo $profesion['id'] ?>"><?php echo $profesion['nombre'] ?></opton>
              <?php endforeach; ?>
            </select> 
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-graduation-cap"></span>
              </div>
            </div>
      </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="editProfesor">Save changes</button>
      </div>
    </div>
  </div>
</div>