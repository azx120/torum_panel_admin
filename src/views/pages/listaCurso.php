<?php  

    $horario = horarioModel::getHorarioById(["id" => $_GET["id_horario"]]);
    $curso = cursoGrupalModel::getCursoGrupalId(["id" => $horario["curso_id"]]);
    $profesor = profesorModel::getProfesorId(["id" => $horario["profesor_id"]]);
    $clientes = horarioModel::getAllClientsHorarios(["id" => $horario["id"]]);
    
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Clientes Del Cursos <?php echo $curso["nombre"]; ?></h1>
            <br/>
            <p>Profesor Encargado: <?php echo $profesor["nombre"]." ".$profesor["apellido"]; ?></p>
            <p>Numero Telefonico: </p>
            <p>Correo Electronico: <?php echo $profesor["mail"]; ?></p>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo $urls; ?>">Home</a></li>
              <li class="breadcrumb-item active">Projects</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table id="dataTablePanel" class="table table-bordered table-striped">
              <thead>
                  <tr>
                      <th style="width: 13%">
                          #
                      </th>
                      <th >
                          Nombre
                      </th>
                   
                      <th>
                         Apellido
                      </th>
                      <th>
                         Correo
                      </th>
                      <th  >
                          Numero
                      </th>
                      <th>
                          Status
                      </th>
                      <th style="width: 20%">
                        Accion
                      </th>
                  </tr>
              </thead>
              <tbody>
              <?php foreach($clientes as $cliente):
                
                if($cliente['status'] == 0){
                  $status = "Inactivo";
                  $status_button = "btn-success";
                  $status_class = "badge badge-danger";
                  $content_button = "Habilitar"; 
              }else{
                  $status = "Activo";
                  $status_button = "btn-danger";
                  $status_class = "badge badge-success";
                  $content_button = "Inabilitar";
              }
              ?>
              
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        <p>
                        <?php echo $cliente['nombre']; ?>
                        </p>
                    </td>
                    <td>
                        <p>
                            <?php echo $cliente['apellido']; ?>
                        </p>
            
                    </td>
                    <td>
                        <p>
                            <?php echo $cliente['mail']; ?>
                        </p>
            
                    </td>
                    <td>
                        <p>
                            <?php echo $cliente['telefono']; ?>
                        </p>
            
                    </td>
                 
                    <td >
                        <span id="cursandoGrupalStatus_<?php echo $cliente['id_grupal']; ?>" class="<?php echo $status_class; ?>"> <?php echo $status; ?></span>
                    </td>
                    <td class="project-actions">
                        <button class="btn <?php echo $status_button ?> btn-sm statusCursoGrupalCliente" id="statusCursandoGrupal_<?php echo $cliente['id_grupal']; ?>" value="<?php echo $cliente['id_grupal']; ?>">
                            <i class="fas fa-trash">
                            </i>
                            <?php echo $content_button ?> 
                        </button>
                    </td>
                </tr>

            <?php endforeach; ?>
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <button class="btn btn-info btn-sm getHorario" data-toggle="modal" data-target="#exampleModalCenter" value="">
                            <i class="fas fa-plus">
                            </i>
                            Agregar Cliente
                        </button>
      <!-- /.card -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Datos del Admininstrador</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post">
          <input class="form-control" type="hidden" id="id_horario" value="<?php echo $horario["id"]; ?>">
          <div class="input-group mb-3">
            <input type="email" class="form-control" name="" id="mailCliente" placeholder="Correo del cliente">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          
          <div class="row">
          </div>
        </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="registerClientCursoGrupal">Save changes</button>
      </div>
    </div>
  </div>
</div>
