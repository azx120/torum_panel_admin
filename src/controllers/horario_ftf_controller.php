<?php
class horarioFtfController{ 

    public static function allHorarios(){

        $horarios = new horarioFtfModel();
        $all_horarios = $horarios -> getAllHorarios();
        return $all_horarios;
    }

    public function getHorarioById($post){

        $horarios = new horarioFtfModel();
        $horari_id = $horarios -> getHorarioById($post);
        return $horari_id;
    }

    public function editHorario($post){

        $horario = new horarioFtfModel();


           $horario_id = $horario -> editHorario($post);

            if ($horario_id == "ok"){
                echo "ok";
            }else {
                echo "error";
            } 
 
    }

    public function statusHorario($post){

        $horarios = new horarioFtfModel();
        $all_horarios = $horarios -> statusHorario($post);
        return $all_horarios;
    }
     
    public function createHoario($post){

        $horarioNew = new horarioFtfModel();
 

         $register = $horarioNew -> newHorario($post);
         return $register;
       
    }


    public function addClientCursoftf($post){
        $users = new UserModel();
        $user = $users -> getUserMail($post);
        if (!empty($user)){
            $curso_gupal = new cursoFtfModel();
           $getUser = $curso_gupal -> getClientCursoFtf(["id" => $user["id"], "horario_id" => $post["id_horario"]]);
           if (empty($getUser)){
                $registerUser = $curso_gupal -> addClientHorario(["id" => $user["id"]],$post);
                return $registerUser;
           }else{
            echo "exist";
           }
        }else{
            echo "no_exist_user";
          }   
    }
 
}



?>
