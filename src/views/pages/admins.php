
  <?php 
    $admins = UserAdminController::allAdmins();
    
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Administradores</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo $urls; ?>">Home</a></li>
              <li class="breadcrumb-item active">Projects</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Projects</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects dataTablePanel">
              <thead>
                  <tr>
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width: 20%">
                          Nombre
                      </th>
                      <th style="width: 30%">
                          Nombre Usuario
                      </th>
                      <th>
                         Correo
                      </th>
                      <th style="width: 8%" class="text-center">
                          ROL
                      </th>
                      <th style="width: 8%" class="text-center">
                          Status
                      </th>
                      <th style="width: 20%">
                        Acciones
                      </th>
                  </tr>
              </thead>
              <tbody>
              <?php foreach($admins as $admin):
                if($admin['status'] == 0){
                  $status = "Inactivo";
                  $status_button = "btn-success";
                  $status_class = "badge badge-danger";
                  $content_button = "Habilitar"; 
              }else{
                  $status = "Activo";
                  $status_button = "btn-danger";
                  $status_class = "badge badge-success";
                  $content_button = "Inabilitar";
              }
              ?>
              
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        <p>
                            <?php echo $admin['name']; ?>
                        </p>
                        <!--<br/>
                        <small>
                            Created 01.01.2019
                        </small>-->
                    </td>
                    <td>
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <?php echo $admin['username']; ?>
                               <!-- <img alt="Avatar" class="table-avatar" src="<?php // echo $urls; ?>src/public/img/avatar.png">-->
                            </li>
                        </ul>
                    </td>
                    <td class="project_progress">
                    <?php echo $admin['mail']; ?>
                    </td>
                    <td class="project_progress">
                    <?php echo $admin['rol']; ?>
                    </td>
                    <td class="project-state" >
                        <span id="adminStatus_<?php echo $admin['id']; ?>" class="<?php echo $status_class; ?>"> <?php echo $status; ?></span>
                    </td>
                    <td class="project-actions text-right">
                    
                        <button class="btn btn-info btn-sm getAdmin" data-toggle="modal" data-target="#exampleModalCenter" value="<?php echo $admin['id']; ?>">
                            <i class="fas fa-pencil-alt">
                            </i>
                            Editar
                        </button>
                        <button class="btn <?php echo $status_button ?> btn-sm statusAdmin" id="statusAdmin_<?php echo $admin['id']; ?>" value="<?php echo $admin['id']; ?>">
                            <i class="fas fa-trash">
                            </i>
                            <?php echo $content_button ?> 
                        </button>
                    </td>
                </tr>
                <?php endforeach ?>
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Datos del Admininstrador</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post">
            <input type="hidden" class="form-control" name="idUserADmin" id="idUserADmin">
          <div class="input-group mb-3">
            
            <input type="text" class="form-control" name="nameUserADmin" id="nameUserADmin" placeholder="Nombre De Ususario">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="nameAdmin" id="nameAdmin" placeholder="Nombre Completo">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="email" class="form-control" name="mailAdmin" id="mailAdmin" placeholder="Correo Electronico">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="passAdmin" id="passAdmin" placeholder="Contraseña">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <select class="form-control" name="rolAdmin" id="rolAdmin" placeholder="Role">
              <option value="ADMIN">ADMIN</opton>
              <option value="SUPER_ADMIN">SUPER ADMIN</opton>
            </select> 
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="editAdmin">Save changes</button>
      </div>
    </div>
  </div>
</div>