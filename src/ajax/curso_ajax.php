<?php

require_once '../controllers/curso_controller.php';
require_once '../models/curso_model.php';

require_once '../controllers/users_controller.php';
require_once '../models/users_model.php';
 
if (isset($_POST["registerCurso"])){

    $curso = cursoController::createCurso($_POST);
   return $curso;
}
	
if (isset($_POST["statusCurso"])){

    $status_curso = cursoController::statusCurso($_POST);
   echo $status_curso;
}

if (isset($_POST["getCursoId"])){

    $curso_by_id = cursoController::getCursoById($_POST);
    
   echo json_encode($curso_by_id);
}

if (isset($_POST["editCurso"])){

    $edit_curso = cursoController::editCurso($_POST);
    
   echo $edit_curso;
}
if (isset($_POST["statusCursandoPlus"])){

    $status_cursandoPlus = cursoModel::statusCursandoPlus($_POST);
   echo $status_cursandoPlus;
}

if (isset($_POST["registerCursandoPlus"])){

    $users = new UserModel();
        $user = $users -> getUserMail($_POST);
        if (!empty($user)){
            $curso = cursoModel::getCursoId(["id"=>$_POST["curso"]]);
            $cursando_plus = cursoModel::createCursandoPlus($user["id"],$curso["numero_videos"], $_POST);
            echo $cursando_plus;
        }else{
            echo "no_exist_user";
        } 
}
?>