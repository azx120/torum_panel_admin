<div class="login-page">  
  <div class="register-box">
    
      <h2 align="center"><b>Registro</b> </br>Nuevo Administrador</h2>
    

    <div class="card">
      <div class="card-body register-card-body">
        <p class="login-box-msg">Rellenar todos los datos</p>

        <form method="post">
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="nameUserADmin" id="nameUserADmin" placeholder="Nombre De Ususario">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="nameAdmin" id="nameAdmin" placeholder="Nombre Completo">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="email" class="form-control" name="mailAdmin" id="mailAdmin" placeholder="Correo Electronico">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="password" class="form-control" name="passAdmin" id="passAdmin" placeholder="Contraseña">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <select class="form-control" name="rolAdmin" id="rolAdmin" placeholder="Role">
              <option value="">Rol</opton>
              <option value="ADMIN">ADMIN</opton>
              <option value="SUPER_ADMIN">SUPER ADMIN</opton>
            </select> 
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          
          <div class="row">
       
            <!-- /.col -->
            <div class="col-12">
              <button type="submit" class="btn btn-primary btn-block" id="registerAdmin">Register</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>
      <!-- /.form-box -->
    </div><!-- /.card -->
  </div>
<!-- /.register-box -->
</div>
