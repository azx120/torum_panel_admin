<?php 

$_SESSION['validate']=null;
$_SESSION['id']=null;
$_SESSION['first_name']=null;
$_SESSION['last_name']=null;
$_SESSION['picture']=null;
$_SESSION['phone']=null;
$_SESSION['email']=null;
$_SESSION['token_access']=null;
$_SESSION['profile_access']=null;
$_SESSION['password']=null;

unset($_SESSION['validate']);
unset($_SESSION['id']);
unset($_SESSION['first_name']);
unset($_SESSION['last_name']);
unset($_SESSION['picture']);
unset($_SESSION['phone']);
unset($_SESSION['email']);
unset($_SESSION['token_access']);
unset($_SESSION['profile_access']);
unset($_SESSION['password']);

session_destroy();

echo '<script>
	
	window.location = "'.$urls.'";

</script>';