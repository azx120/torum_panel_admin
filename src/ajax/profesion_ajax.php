<?php

require_once '../controllers/profesion_controller.php';
require_once '../models/profesion_model.php';


if (isset($_POST["registerProfesion"])){

    $new_profesion = profesionController::createProfesion($_POST);
   echo $new_profesion;
}

if (isset($_POST["statusProfesion"])){

    $status_profesion = profesionController::statusProfesion($_POST);
   echo $status_profesion;
}

if (isset($_POST["getProfesionId"])){

    $profesion_by_id = profesionController::getProfesionById($_POST);
    
   echo json_encode($profesion_by_id);
}

if (isset($_POST["editProfesion"])){

    $editProfesion = profesionController::editProfesion($_POST);
    
   echo $editProfesion;
}
?>