<?php 
require_once "conexion.php";

class cursoFtfModel{

	//crear nuevo usuario
	public function getAllCursosFtf(){

		$stmt = Conexion::conection()->prepare("SELECT * FROM cursando_ftf");

		$stmt->execute();
		return $stmt->fetchall();

	}

	public function statusCursoFtf($post){

		$stmt = Conexion::conection()->prepare("UPDATE cursando_ftf SET status = NOT status WHERE id = :id");
		$stmt->bindParam(":id", $post["status"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

	}

	public function cursoFtfEdit($post){
		

		$stmt = Conexion::conection()->prepare("UPDATE cursando_ftf SET nombre = :nombre, profesion_id = :profesionId, boton_ftf = :boton_ftf WHERE id = :id");
		
		$stmt->bindParam(":id", $post["idCurso"], PDO::PARAM_INT);
		$stmt->bindParam(":nombre", $post["nameCurso"], PDO::PARAM_STR);
		$stmt->bindParam(":profesionId", $post["profesionCurso"], PDO::PARAM_STR);
		$stmt->bindParam(":boton_ftf", $post["botonFtf"], PDO::PARAM_STR);
	
	
			
		if($stmt -> execute()){

			return "ok";	
		
		}else{

			return "error";	

		}

	}
	
	//crear nuevo curso
	public function newCursoFtf($post){

		$stmt = Conexion::conection()->prepare("INSERT INTO cursando_ftf (nombre,  profesion_id, boton_ftf, status) VALUES (:nombre, :profesionId,:boton_ftf, 1)");


		$stmt->bindParam(":nombre", $post["nameCurso"], PDO::PARAM_STR);
		$stmt->bindParam(":profesionId", $post["profesionCurso"], PDO::PARAM_STR);
		$stmt->bindParam(":boton_ftf", $post["botonFtf"], PDO::PARAM_STR);
	

		if($stmt->execute()){
			return "ok";
		}else{
			return "error";
		}
	}
	
	
	//Metodos de consulta
	public static function getCursoFtfId($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM cursando_ftf WHERE id = :id");

		$stmt -> bindParam(":id", $post['id'], PDO::PARAM_INT);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}
	
		//Metodos de consulta
		public function getCursoFtfByProfesion($post){

			$stmt = Conexion::conection()->prepare("SELECT * FROM cursando_ftf WHERE profesion_id = :profesion_id AND status = 1");
	
			$stmt -> bindParam(":profesion_id", $post['profesion_id'], PDO::PARAM_INT);
	
			$stmt ->execute();
			
			return $stmt ->fetchAll();
	
			$stmt = null;
	
		}
    
    //Metodos de consulta
	public function getCursoFtf($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM cursando_ftf WHERE nombre = :nombre");

		$stmt -> bindParam(":nombre", $post['nameCurso'] , PDO::PARAM_STR);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}

	  //Metodos de consulta
	  public function getClientCursoFtf($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM cursando_ftf WHERE user_id = :id AND horario_id = :horario_id");

		$stmt -> bindParam(":id", $post['id'] , PDO::PARAM_INT);
		$stmt -> bindParam(":horario_id", $post['horario_id'] , PDO::PARAM_INT);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}

	//Metodos de consulta
	public static function addClientHorario($id, $post){

		$fecha = date("Y-m-d");
		$stmt = Conexion::conection()->prepare("INSERT INTO cursando_ftf (user_id, horario_id,status,created) VALUES (:user_id, :horario_id, 1,:created)");

		$stmt -> bindParam(":user_id", $id['id'], PDO::PARAM_INT);
		$stmt -> bindParam(":horario_id", $post['id_horario'], PDO::PARAM_INT);
		$stmt -> bindParam(":created", $fecha, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}
	}

}
?>
