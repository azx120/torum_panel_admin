<?php
require_once "conexion.php";

class cursoModel{

	//crear nuevo usuario
	public function getAllCursos(){

		$stmt = Conexion::conection()->prepare("SELECT * FROM cursos");

		$stmt->execute();
		return $stmt->fetchall();

	}

		//crear nuevo usuario
		public static function getAllCursos_plus(){

			$stmt = Conexion::conection()->prepare("SELECT * FROM cursos WHERE plus = 1");
	
			$stmt->execute();
			return $stmt->fetchall();
	
		}

	public static function getAllCursando_plus(){

		$stmt = Conexion::conection()->prepare("SELECT * FROM cursando_plus");

		$stmt->execute();
		return $stmt->fetchall();

	}

	public function statusCursandoPlus($post){

		$stmt = Conexion::conection()->prepare("UPDATE cursando_plus SET status = NOT status WHERE id = :id");
		$stmt->bindParam(":id", $post["status"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

	}

	//crear nuevo curso
	public function createCursandoPlus($id_user, $videos, $post){

		$fecha = date("Y-m-d");

		$stmt = Conexion::conection()->prepare("INSERT INTO cursando_plus (user_id, curso_id,status, vencido,nro_video,video_dia,completado,created,plan) VALUES (:user_id,:curso_id, 1, 0,:nro_video,1,0,:created,:plan)");


		$stmt->bindParam(":user_id", $id_user, PDO::PARAM_INT);
		$stmt->bindParam(":curso_id", $post["curso"], PDO::PARAM_INT);
		$stmt->bindParam(":plan", $post["planCurso"], PDO::PARAM_INT);
		$stmt->bindParam(":nro_video", $videos, PDO::PARAM_INT);
		$stmt->bindParam(":created", $fecha, PDO::PARAM_STR);

		if($stmt->execute()){
			return "ok";
		}else{
			return "error";
		}
	}

	public function statusCurso($post){

		$stmt = Conexion::conection()->prepare("UPDATE cursos SET status = NOT status WHERE id = :id");
		$stmt->bindParam(":id", $post["status"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

	}

	public function cursoEdit($post){
		
		$stmt = Conexion::conection()->prepare("UPDATE cursos SET nombre = :nombre, numero_videos= :numero_videos,descripcion = :descripcion, plus = :plus, profesion_id = :profesionId WHERE id = :id");
		
		$stmt->bindParam(":id", $post["idCurso"], PDO::PARAM_INT);
		$stmt->bindParam(":nombre", $post["nameCurso"], PDO::PARAM_STR);
		$stmt->bindParam(":numero_videos", $post["videosCurso"], PDO::PARAM_INT);
		$stmt->bindParam(":plus", $post["plus"], PDO::PARAM_INT);
		$stmt->bindParam(":descripcion", $post["editor1"], PDO::PARAM_STR);
        $stmt->bindParam(":profesionId", $post["profesionCurso"], PDO::PARAM_STR);
	
	
			
		if($stmt -> execute()){

			return "ok";	
		
		}else{

			return "error";	

		}

	}
	
	//crear nuevo curso
	public function newCurso($post){

		$stmt = Conexion::conection()->prepare("INSERT INTO cursos (nombre, numero_videos,descripcion, plus, profesion_id, status) VALUES (:nombre,:numero_videos, :descripcion,:plus, :profesionId, 1)");


		$stmt->bindParam(":nombre", $post["nameCurso"], PDO::PARAM_STR);
		$stmt->bindParam(":numero_videos", $post["videosCurso"], PDO::PARAM_INT);
		$stmt->bindParam(":plus", $post["plusCursos"], PDO::PARAM_STR);
		$stmt->bindParam(":descripcion", $post["editor1"], PDO::PARAM_STR);
        $stmt->bindParam(":profesionId", $post["profesionCurso"], PDO::PARAM_STR);
	

		if($stmt->execute()){
			return "ok";
		}else{
			return "error";
		}
	}
	
	
	//Metodos de consulta
	public static function getCursoId($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM cursos WHERE id = :id");

		$stmt -> bindParam(":id", $post['id'], PDO::PARAM_INT);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}
	
		//Metodos de consulta
		public function getCursoByProfesion($post){

			$stmt = Conexion::conection()->prepare("SELECT * FROM cursos WHERE profesion_id = :profesion_id");
	
			$stmt -> bindParam(":profesion_id", $post['profesion_id'], PDO::PARAM_INT);
	
			$stmt ->execute();
			
			return $stmt ->fetchAll();
	
			$stmt = null;
	
		}
    
    //Metodos de consulta
	public function getCurso($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM cursos WHERE nombre = :nombre");

		$stmt -> bindParam(":nombre", $post['nameCurso'] , PDO::PARAM_STR);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}

}
?>
