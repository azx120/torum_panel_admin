<?php
require_once "conexion.php";

class profesorModel{

	//crear nuevo usuario
	public function getAllProfesor(){

		$stmt = Conexion::conection()->prepare("SELECT * FROM profesores");

		$stmt->execute();
		return $stmt->fetchall();

	}

	public function statusProfesor($post){

		$stmt = Conexion::conection()->prepare("UPDATE profesores SET status = NOT status WHERE id = :id");
		$stmt->bindParam(":id", $post["status"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

	}

	public function profesorEdit($post){


		$stmt = Conexion::conection()->prepare("UPDATE profesores SET nombre = :nombre, apellido = :apellido, mail = :mail, password = :password,especialidad = :especialidad, descripcion = :descripcion, boton_ftf = :boton_ftf,img = :img, profesion_id = :profesionId WHERE id = :id");
		
		$stmt->bindParam(":id", $post["idProfesor"], PDO::PARAM_INT);
		$stmt->bindParam(":nombre", $post["nameProfesor"], PDO::PARAM_STR);
		$stmt->bindParam(":apellido", $post["apellidoProfesor"], PDO::PARAM_STR);
        $stmt->bindParam(":mail", $post["mailProfesor"], PDO::PARAM_STR);
		$stmt->bindParam(":password", $post["passProfesor"], PDO::PARAM_STR);
		$stmt->bindParam(":especialidad", $post["especialidad"], PDO::PARAM_STR);
		$stmt->bindParam(":descripcion", $post["editor1"], PDO::PARAM_STR);
		$stmt->bindParam(":boton_ftf", $post["botonFtf"], PDO::PARAM_STR);
		$stmt->bindParam(":profesionId", $post["profesionProfesor"], PDO::PARAM_STR);
		$stmt->bindParam(":img", $post["imgPersonHidden"], PDO::PARAM_STR);
	
	
			
		if($stmt -> execute()){

			return "ok";	
		
		}else{

			return "error";	

		}

	}
	
	//crear nueva profesor
	public function newProfesor($post){

		$nombre_archivo = $_FILES['imgPerson']['name'];

		$stmt = Conexion::conection()->prepare("INSERT INTO profesores (nombre, apellido, mail, password, especialidad, descripcion, boton_ftf, profesion_id,img, status) VALUES (:nombre, :apellido, :mail, :password, :especialidad, :descripcion, :boton_ftf, :profesionId,:img, 1)");


		$stmt->bindParam(":nombre", $post["nameProfesor"], PDO::PARAM_STR);
		$stmt->bindParam(":apellido", $post["apellidoProfesor"], PDO::PARAM_STR);
        $stmt->bindParam(":mail", $post["mailProfesor"], PDO::PARAM_STR);
		$stmt->bindParam(":password", $post["passProfesor"], PDO::PARAM_STR);
		$stmt->bindParam(":especialidad", $post["especialidad"], PDO::PARAM_STR);
		$stmt->bindParam(":descripcion", $post["editor1"], PDO::PARAM_STR);
		$stmt->bindParam(":boton_ftf", $post["botonFtf"], PDO::PARAM_STR);
		$stmt->bindParam(":profesionId", $post["profesionProfesor"], PDO::PARAM_STR);
		$stmt->bindParam(":img", $nombre_archivo, PDO::PARAM_STR);
	

		if($stmt->execute()){
			return "ok";
		}else{
			return "error";
		}
	}
	
	
	//Metodos de consulta
	public static function getProfesorId($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM profesores WHERE id = :id");

		$stmt -> bindParam(":id", $post['id'], PDO::PARAM_INT);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}
	
		//Metodos de consulta
		public function getProfesorByProfesion($post){

			$stmt = Conexion::conection()->prepare("SELECT * FROM profesores WHERE profesion_id = :profesion_id AND status = 1");
	
			$stmt -> bindParam(":profesion_id", $post['profesion_id'], PDO::PARAM_INT);
	
			$stmt ->execute();
			
			return $stmt ->fetchAll();
	
			$stmt = null;
	
		}
    
    //Metodos de consulta
	public function getProfesor($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM profesores WHERE mail = :mail AND profesion_id = :profesion_id");

		$stmt -> bindParam(":mail", $post['mailProfesor'] , PDO::PARAM_STR);
		$stmt -> bindParam(":profesion_id", $post['profesionProfesor'] , PDO::PARAM_STR);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}

}
?>
