<?php
class UserAdminController{

    public static function allAdmins(){

        $admins = new UserAdminModel();
        $all_admin = $admins -> getAllAdmin();
        return $all_admin;
    }

    public function getAdminById($post){

        $admins = new UserAdminModel();
        $admin_id = $admins -> getAdminId($post);
        return $admin_id;
    }

    public function editAdmin($post){

        $admins = new UserAdminModel();

        $user = $admins -> getUser($post);

        if ($user['id'] == $post['idAdmin'] || empty($user)){
            $admin_id = $admins -> adminEdit($post);
            if ($admin_id == "ok"){
                echo "ok";
            }else {
                echo "error";
            }
        }else{
            echo "exist";
        }    
    }

    public function statusAdmin($post){

        $admins = new UserAdminModel();
        $all_admin = $admins -> statusAdmin($post);
        return $all_admin;
    }
     
    public function createAdminUser($post){

        $usuarios = new UserAdminModel();
        $user = $usuarios -> getUser($post);
        if ($user == "" && $user == null && $user == false){
            $register = $usuarios -> newAdminUser($post);
            if ($register == "ok"){
                echo "ok";
            }else {
                echo "error";
            }
        }else{
            echo "exist";
        }    
    }
      

     
     
    public function loginUserAdmin($post){
        $usuarios = new UserAdminModel();
        $usu = $usuarios -> getLoginUser($post);
        if (!empty($usu)){
            if($usu["status"] == 1){
                session_start();
                $_SESSION["validate"] = "ok";
                $_SESSION['id'] = $usu['id'];
                $_SESSION['name'] = $usu['name'];
                $_SESSION['rol'] = $usu['rol'];
                echo "ok";
            }else{
                echo "disabled";
            }
            
        }else {
            echo "error";
        }
    }
 
}



?>
