<?php

require_once '../controllers/user_admin_controller.php';
require_once '../models/user_admin_model.php';

if (isset($_POST["usernameLogin"])){

    $user = UserAdminController::loginUserAdmin($_POST);
   echo $user;
}

if (isset($_POST["registerAdmin"])){

    $user_admin = UserAdminController::createAdminUser($_POST);
   echo $user_admin;
}

if (isset($_POST["statusAdmin"])){

    $status_admin = UserAdminController::statusAdmin($_POST);
   echo $status_admin;
}

if (isset($_POST["getAdminId"])){

    $admin_by_id = UserAdminController::getAdminById($_POST);
    
   echo json_encode($admin_by_id);
}

if (isset($_POST["editAdmin"])){

    $edit_admin = UserAdminController::editAdmin($_POST);
    
   echo $edit_admin;
}
?>