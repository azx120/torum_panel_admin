<?php 

/*=============================================
MENSAJES
=============================================*/

class CtrMenssages
{
	/*=============================================
	MOSTRAR MENSAJES 
	=============================================*/
		
    public static function ctrViewMenssage($item, $value)
    {
        $table = "tc_menssages";

        $response = MdlMenssages::mdlViewMenssage($table, $item, $value);

        return $response;
    }

    	/*=============================================
	MOSTRAR MENSAJES CHAT
	=============================================*/
		
    public static function ctrViewMenssageChat($item, $value)
    {
        $table = "tc_menssages";
        $item2 = "transmitter";

        $response = MdlMenssages::mdlViewchat($table, $item, $value, $item2);

        return $response;
    }

    /*=============================================
    MOSTRAR MENSAJES 
    =============================================*/
        
    public static function ctrViewMenssageList($item, $value)
    {
        $table = "tc_menssages";

        $response = MdlMenssages::mdlViewMenssageList($table, $item, $value);

        return $response;
    }


    /*=============================================
    MOSTRAR MENSAJES 
    =============================================*/
        
    public static function ctrViewMenssagePending($item, $value, $status)
    {
        $table = "tc_menssages";

        $response = MdlMenssages::mdlViewMenssagePending($table, $item, $value, $status);

        return $response;
    }


    /*=============================================
    ENVIAR NUEVOEMNSAJE 
    =============================================*/
        
    public static function ctrNewMessage($dates)
    {
        $table = "tc_menssages";

        $response = MdlMenssages::mdlNewMessage($table, $dates);

        return $response;
    }

    public static function crtRequestChat($dates){
        $table = "tc_menssages";
        $response = MdlMenssages::mdlRequestChat($table, $dates);
        return $response;
    }
}