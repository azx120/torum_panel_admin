<?php 

require_once '../controllers/curso_grupal_controller.php';
require_once '../models/curso_grupal_model.php';

require_once '../controllers/users_controller.php';
require_once '../models/users_model.php';

 
if (isset($_POST["registerCursoGrupal"])){

    $curso = cursoGrupalController::createCursoGrupal($_POST);
   echo $curso;
}
	
if (isset($_POST["statusCursoGrupal"])){

    $status_curso = cursoGrupalController::statusCursoGrupal($_POST);
   echo $status_curso;
}

if (isset($_POST["getCursoGrupalId"])){

    $curso_by_id = cursoGrupalController::getCursoGrupalById($_POST);
    
   echo json_encode($curso_by_id);
}

if (isset($_POST["editCursoGrupal"])){

    $edit_curso = cursoGrupalController::editCursoGrupal($_POST);
    
   echo $edit_curso;
}

if (isset($_POST["addClientCursoGrupal"])){

    $edit_curso = cursoGrupalController::addClientCursoGrupal($_POST);
    
   echo $edit_curso;
}


if (isset($_POST["statusCursoGrupalCliente"])){

    $status_curso = cursoGrupalModel::statusCursoGrupalCliente($_POST);
   echo $status_curso;
}
?>