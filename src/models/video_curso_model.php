<?php
require_once "conexion.php";

class videoCursoModel{

	//crear nuevo usuario
	public static function getAllVideosCurso($post){
	
		$stmt = Conexion::conection()->prepare("SELECT * FROM videos_curso WHERE curso_id = :profesion_id ORDER BY nro_video ASC");
	
		$stmt -> bindParam(":profesion_id", $post['id'], PDO::PARAM_INT);

		$stmt ->execute();
		
		return $stmt ->fetchAll();

		$stmt = null;

	}

	public function statusVideoCurso($post){

		$stmt = Conexion::conection()->prepare("UPDATE videos_curso SET status = NOT status WHERE id = :id");
		$stmt->bindParam(":id", $post["status"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

	}

	public function cursoEdit($post){
		

		$stmt = Conexion::conection()->prepare("UPDATE videos_curso SET titulo = :titulo, nro_video= :nro_video, video_id = :video_id WHERE id = :id");
		
		$stmt->bindParam(":id", $post["id_video_curso"], PDO::PARAM_INT);
		$stmt->bindParam(":titulo", $post["titleVideoCurso"], PDO::PARAM_STR);
		$stmt->bindParam(":nro_video", $post["nroVideosCurso"], PDO::PARAM_INT);
        $stmt->bindParam(":video_id", $post["idVideo"], PDO::PARAM_STR);
	
	
			
		if($stmt -> execute()){

			return "ok";	
		
		}else{

			return "error";	

		}

	}
	
	//crear nuevo curso
	public function newCurso($post){

		$stmt = Conexion::conection()->prepare("INSERT INTO videos_curso (titulo, nro_video, video_id, curso_id, status) VALUES (:titulo,:nro_video, :video_id, :curso_id, 1)");


		$stmt->bindParam(":titulo", $post["titleVideoCurso"], PDO::PARAM_STR);
		$stmt->bindParam(":nro_video", $post["nroVideosCurso"], PDO::PARAM_INT);
        $stmt->bindParam(":video_id", $post["idVideo"], PDO::PARAM_STR);
        $stmt->bindParam(":curso_id", $post["id_curso"], PDO::PARAM_STR);
	

		if($stmt->execute()){
			return "ok";
		}else{
			return "error";
		}
	}
	
	
	//Metodos de consulta
	public static function getCursoId($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM videos_curso WHERE id = :id");

		$stmt -> bindParam(":id", $post['id'], PDO::PARAM_INT);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}
	
		//Metodos de consulta
		public function getCursoByProfesion($post){

			$stmt = Conexion::conection()->prepare("SELECT * FROM videos_curso WHERE profesion_id = :profesion_id");
	
			$stmt -> bindParam(":profesion_id", $post['profesion_id'], PDO::PARAM_INT);
	
			$stmt ->execute();
			
			return $stmt ->fetchAll();
	
			$stmt = null;
	
		}
    
    //Metodos de consulta
	public function getVideoCurso($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM videos_curso WHERE curso_id = :curso_id AND nro_video = :nro_video");

		$stmt -> bindParam(":curso_id", $post['id_curso'] , PDO::PARAM_STR);
		$stmt -> bindParam(":nro_video", $post['nroVideosCurso'] , PDO::PARAM_INT);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}

}
?>
