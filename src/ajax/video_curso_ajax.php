<?php

require_once '../controllers/video_curso_controller.php';
require_once '../models/video_curso_model.php';

 
if (isset($_POST["register_video_curso"])){

    if ($_POST["register_video_curso"] == "true"){
        $curso = videoCursoController::createVideoCurso($_POST);
        echo $curso;
    }else  if ($_POST["register_video_curso"] == "false"){
        $edit_curso = videoCursoController::editVideoCurso($_POST);
        echo $edit_curso;
    }    
}
	
if (isset($_POST["statusVideoCurso"])){

    $status_curso = videoCursoController::statusVideoCurso($_POST);
   echo $status_curso;
}

if (isset($_POST["getCursoId"])){

    $curso_by_id = videoCursoController::getCursoById($_POST);
    
   echo json_encode($curso_by_id);
}

?>