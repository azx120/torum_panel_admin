<?php 
  $profesiones = profesionController::getAllProfesionRegister();
  
?>
<div class="login-page">  
  <?php
        if(empty($profesiones)){
          echo "<p class='login-box-msg' style='color:red; font-size:20px;'>Profesiones vacias, Debes registrar alguna!</p>";
        }
  ?>
  <div class="register-box">
    
    <h2 align="center"><b>Registro</b> </br>Nuevo Curso Grupal</h2>
    
    <div class="card">
      <div class="card-body register-card-body">
        <p class="login-box-msg">Rellenar todos los datos</p>
   
        <form method="post">
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="" id="nameCurso" placeholder="Nombre Del Curso">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <textarea class="form-control" name="" id="botonFtf" placeholder="Boton de pago mercado pago"></textarea>
        
          </div>
          <div class="input-group mb-3">
            <select class="form-control" name="rolAdmin" id="profesionCurso" placeholder="Role">
              <option value="">Profesion</opton>
              <?php foreach($profesiones as $profesion): ?>
                <option value="<?php echo $profesion['id'] ?>"><?php echo $profesion['nombre'] ?></opton>
              <?php endforeach; ?>
            </select> 
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          
          <div class="row">
       
            <!-- /.col -->
            <div class="col-12">
              <button type="submit" class="btn btn-primary btn-block" id="registerCursoGrupal">Register</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>
      <!-- /.form-box -->
    </div><!-- /.card -->
  </div>
<!-- /.register-box -->
</div>

