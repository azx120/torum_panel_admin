<?php
class cursoController{

    public static function allCursos(){

        $prefesiones = new cursoModel();
        $all_prefesiones = $prefesiones -> getAllCursos();
        return $all_prefesiones;
    }

    public function getCursoById($post){

        $profesores = new cursoModel();
        $profesor_id = $profesores -> getCursoId($post);
        return $profesor_id;
    }

    public function editCurso($post){

        $profesor = new cursoModel();

        $getProfesor = $profesor -> getCurso($post);

        if ($getProfesor['id'] == $post['idCurso'] || empty($getProfesor)){

           $profesor_id = $profesor -> cursoEdit($post);

            if ($profesor_id == "ok"){
                echo "ok";
            }else {
                echo "error";
            }
        }else{
            echo "exist";
        } 
    }

    public function statusCurso($post){

        $profesores = new cursoModel();
        $all_profesores = $profesores -> statusCurso($post);
        return $all_profesores;
    }
     
    public static function createCurso($post){

        $cursoNew = new cursoModel();
        $curso = $cursoNew -> getCurso($post);
        if ($curso == "" && $curso == null && $curso == false){
            $register = $cursoNew -> newCurso($post);
            if ($register == "ok"){
                echo "ok";
            }else {
                echo "error";
            }
        }else{
            echo "exist";
        }    
    }
 
}



?>
