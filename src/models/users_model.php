<?php
require_once "conexion.php";

class UserModel{

	//crear nuevo usuario
	public static function getAllUsers(){

		$stmt = Conexion::conection()->prepare("SELECT * FROM clientes");

		$stmt->execute();
		return $stmt->fetchall();

	}

	
	//crear nuevo usuario
	public function editUser($post){
		
		$stmt = Conexion::conection()->prepare("UPDATE clientes SET nombre = :nombre, apellido = :apellido, mail = :mail, telefono = :telefono WHERE id = :id");

		$stmt->bindParam(":id", $post["idClient"], PDO::PARAM_INT);
		$stmt->bindParam(":nombre", $post["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":apellido", $post["apellido"], PDO::PARAM_STR);
		$stmt->bindParam(":mail", $post["mail"], PDO::PARAM_STR);
		$stmt->bindParam(":telefono", $post["telefono"], PDO::PARAM_STR);
		
		if($stmt->execute()){
			return "ok";
		}else{
			return "error_user";
		}
	}

	//crear nuevo usuario
	public function newUserGoogle($post){

		$fecha = date("Y-m-d");
		$stmt = Conexion::conection()->prepare("INSERT INTO clientes 
		(nombre, mail,img, free, plus_referido, status, google, fecha_registro)VALUES(:nombre, :mail,:img, 1, 0, 1,1,:fecha_registro)");

		$stmt->bindParam(":nombre", $post["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":mail", $post["correo"], PDO::PARAM_STR);
		$stmt->bindParam(":img", $post["img"], PDO::PARAM_STR);
		$stmt->bindParam(":fecha_registro", $fecha, PDO::PARAM_STR);

		if($stmt->execute()){
			return "ok";
		}else{
			return "error_user";
		}
	}
	//crear datos de usuario
	public function registerDatesUser($post, $id){
	
		$fecha = date("Y-m-d");
		$stmt = Conexion::conection()->prepare("INSERT INTO datos_cliente 
		(peso, altura, id_cliente, registro)VALUES(:peso, :altura, :id_cliente, :registro)");

		$stmt->bindParam(":peso", $post["peso"], PDO::PARAM_STR);
		$stmt->bindParam(":altura", $post["altura"], PDO::PARAM_STR);
		$stmt->bindParam(":id_cliente", $id, PDO::PARAM_STR);
		$stmt->bindParam(":registro", $fecha, PDO::PARAM_STR);

		if($stmt->execute()){
			return "ok_all";
		}else{
			return "error_datas";
		}
	}
	
	//Metodos de consulta
	public function getUser($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM clientes WHERE mail = :mail");

		$stmt -> bindParam(":mail", $post['mail'], PDO::PARAM_STR);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}

	//Metodos de consulta
	public function getUserMail($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM clientes WHERE mail = :mail");

		$stmt -> bindParam(":mail", $post['correo_client'], PDO::PARAM_STR);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}

		//Metodos de consulta
		public function getUserById($post){

			$stmt = Conexion::conection()->prepare("SELECT * FROM clientes WHERE id = :id");
	
			$stmt -> bindParam(":id", $post['id'], PDO::PARAM_STR);
	
			$stmt ->execute();
			
			return $stmt ->fetch();
	
			$stmt = null;
	
		}

	
	//Metodos de consulta
	public function getAdminId($post){

		$stmt = Conexion::conection()->prepare("SELECT * FROM user_admin WHERE id = :id");

		$stmt -> bindParam(":id", $post['id'], PDO::PARAM_STR);

		$stmt ->execute();
		
		return $stmt ->fetch();

		$stmt = null;

	}

    //Metodos de consulta
    public function getLoginUser($post){

		$pass = crypt($post['password'], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
        $stmt = Conexion::conection()->prepare("SELECT * FROM clientes WHERE mail = :mail AND password = :password");

		$stmt -> bindParam(":mail", $post['usernameLogin'], PDO::PARAM_STR);
		$stmt -> bindParam(":password", $pass, PDO::PARAM_STR);
		


		$stmt ->execute();
      
		return $stmt ->fetch();
		

		$stmt = null;

	}
	
	   //Metodos de consulta
	   public function getLoginGoogleUser($post){

        $stmt = Conexion::conection()->prepare("SELECT * FROM clientes WHERE mail = :mail");

		$stmt -> bindParam(":mail", $post['mail'], PDO::PARAM_STR);


		$stmt ->execute();
      
		return $stmt ->fetch();
		

		$stmt = null;

	}
	public function updateStateUser($campo,$id){
		
		$stmt = Conexion::conection()->prepare("UPDATE clientes SET $campo = NOT $campo WHERE id = :id");
		
		$stmt->bindParam(":id", $id["status"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";	
		
		}else{

			return "error";	

		}
	

	}
}
?>
