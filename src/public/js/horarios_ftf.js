$( document ).ready(function() { 

    $("#profesion_id").on('change',  function(event) {
        var id = $(this).val();
        var url = $("#url").val(); 
        $('.options_cursos').remove();
        if (id == ""){
            $('#curso_id').attr('disabled', true);
            $('#profesor_id').attr('disabled', true);
        }else{
            $('#curso_id').attr('disabled', false);
            $('#profesor_id').attr('disabled', false);
       
      
       
            var ajax = new XMLHttpRequest();

            var URL = url +'src/ajax/horario_ftf_ajax.php';
            var method = "POST";

            ajax.onreadystatechange = function() {
                if (ajax.readyState == 4 && ajax.status == 200) {
                    var response = ajax.responseText;

                    if (response !== "no_curso" && response !== "no_profesor") {
                        var obj = jQuery.parseJSON(response);

                        $.each(obj.cursos, function(key,value) {
                            $('<option class="options_cursos" value="'+value.id +'">'+value.nombre+'</opton>').appendTo($('#curso_id'));
                        })
                        $.each(obj.profesores, function(key,value) {
                            $('<option class="options_cursos" value="'+value.id +'">'+value.nombre+'</opton>').appendTo($('#profesor_id'));
                        })
                    }else if(response == "no_curso"){
                        $('#curso_id').attr('disabled', true);
                        $('#profesor_id').attr('disabled', true);
                        swal('Error', 'Error, No hay curso grupal para esta profesion', 'error');
                    }else if(response == "no_profesor"){
                        $('#curso_id').attr('disabled', true);
                        $('#profesor_id').attr('disabled', true);
                        swal('Error', 'Error, No hay profesor para esta profesion', 'error');
                    }
                }        
            }
            ajax.open(method, URL, true);
            ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            ajax.send("getDatesProfesion=true&profesion_id="+ id);
        }
    })

    $("#registerHorario_ftf").on('click',  function(event) {
        event.preventDefault();
    
        var nro_client = $("#nro_client").val();
        var linkZoom = $("#linkZoom").val(); 
        var hora_inicio = $("#hora_inicio").val();
        var hora_finalizar = $("#hora_finalizar").val();
        var profesor_id = $("#profesor_id").val();
        var diasemana = $("#diasemana").val();
        var curso_id = $("#curso_id").val();
        var url = $("#url").val();


        if (hora_inicio =='') {
            swal('Error', 'Elija una hora inicial!', 'error');
        
            return false;
        };
     
        if (hora_finalizar =='') {
            swal('Error', 'Elija una hora finalizar!', 'error');
        
            return false;
        };
        if (nro_client =='') {
            swal('Error', 'Error numero de clientes vacio!', 'error');
        
            return false;
        };
        if (diasemana =='') {
            swal('Error', 'Error dias de semana vacio!', 'error');
        
            return false;
        };
        
        if (linkZoom =='') {
            swal('Error', 'Error Enlace Zoom vacio!', 'error');
        
            return false;
        };

        if (profesor_id =='') {
            swal('Error', 'Error no se ha selecinado un profesor!', 'error');
        
            return false;
        };
        if (curso_id =='') {
            swal('Error', 'Error no se ha selecinado un curso!', 'error');
        
            return false;
        };
    

        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/horario_ftf_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
             
                if (response == "ok") {
                    swal({
                        title: "¡OK!",
                        text: "Horario Face To Face Registrado exitosamente!",
                        type: "success",
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    },
                    
                    function(isConfirm) {
                        if (isConfirm) {
                            window.location.reload();
                    
                        }
                    });
                }else{

					swal('Error', 'Error no se ha podido registrar el horario!', 'error');

				};
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("registerHorario=true&hora_inicio="+ hora_inicio +"&hora_finalizar="+hora_finalizar +"&linkZoom="+ linkZoom + "&profesor_id="+ profesor_id+ "&curso_id="+ curso_id + "&nro_client="+ nro_client+ "&diasemana="+diasemana);
    

    })

    $(".statusHorarioFtf").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();

       var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/horario_ftf_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    var st = $("#horarioFtfStatus_"+id).attr("class");
                    if(st == "badge badge-danger"){
                      
                        $("#horarioFtfStatus_"+id).attr("class","badge badge-success");
                        $("#horarioFtfStatus_"+id).text("Activo");
                        
                     
                        $("#statusHorarioFtf_"+id).addClass("btn-danger");
                        $("#statusHorarioFtf_"+id).removeClass("btn-success");
                        $("#statusHorarioFtf_"+id).text("Inhabilitar ");
                 

                    }else if(st == "badge badge-success"){

                        $("#horarioFtfStatus_"+id).attr("class","badge badge-danger");
                        $("#horarioFtfStatus_"+id).text("Inactivo");
                 

                        $("#statusHorarioFtf_"+id).addClass("btn-success");
                        $("#statusHorarioFtf_"+id).removeClass("btn-danger");
                        $("#statusHorarioFtf_"+id).text("Habilitar");
                        

                    }
                    console.log(st);
                }else if(response == "error"){
                    console.log("erro");
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("statusHorario=true&status="+ id);

    })

    $(".getHorarioFtf").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();
        var ajax = new XMLHttpRequest();
        $('.options_cursos').remove();
        var URL = url +'src/ajax/horario_ftf_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response !== "") {
                    var obj = jQuery.parseJSON(response);
                    $('#curso_id').attr('disabled', false);
                    $('#profesor_id').attr('disabled', false);
                    $("#id_horaio").val(obj.id);
                    $("#nro_client").val(obj.nroClient);
                    $("#linkZoom").val(obj.link_zoom);
                    $("#hora_inicio").val(obj.hora_inicio);
                    $("#hora_finalizar").val(obj.hora_finalizar);
                    $("#diasemana").val(obj.diasemana)
                    $('<option class="options_cursos" value="'+obj.curso_id+'">'+obj.nombre_curso+'</opton>').appendTo($('#curso_id'));
                    $('<option class="options_cursos" value="'+obj.profesor_id+'">'+obj.nombre_profesor+'</opton>').appendTo($('#profesor_id'));
                   
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("getHoraioId=true&id="+ id);
    })

    $("#editHoraio_ftf").on('click',  function(event) {
        event.preventDefault();
        var idHorario = $("#id_horaio").val();
        var hora_inicio = $("#hora_inicio").val();
        var hora_finalizar = $("#hora_finalizar").val();
        var nro_client = $("#nro_client").val();
        var linkZoom = $("#linkZoom").val();
        var profesor_id = $("#profesor_id").val();
        var curso_id = $("#curso_id").val();
        var diasemana = $("#diasemana").val();
        var url = $("#url").val();

        
        if (hora_inicio =='') {
            swal('Error', 'Elija una hora inicial!', 'error');
        
            return false;
        };
     
        if (hora_finalizar =='') {
            swal('Error', 'Elija una hora finalizar!', 'error');
        
            return false;
        };
        if (nro_client =='') {
            swal('Error', 'Error numero de clientes vacio!', 'error');
        
            return false;
        };

        
        if (linkZoom =='') {
            swal('Error', 'Error Enlace Zoom vacio!', 'error');
        
            return false;
        };
        if (profesor_id =='') {
            swal('Error', 'Error no se ha selecinado un profesor!', 'error');
        
            return false;
        };
        if (curso_id =='') {
            swal('Error', 'Error no se ha selecinado un curso!', 'error');
        
            return false;
        };
        
        if (diasemana =='') {
            swal('Error', 'Error dias de semana vacio!', 'error');
        
            return false;
        };
    

        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/horario_ftf_ajax.php';
        var method = "POST";
 
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    swal({
                        title: "¡OK!",
                        text: "Horario Face To Face Editado exitosamente!",
                        type: "success",
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    },
                    
                    function(isConfirm) {
                        if (isConfirm) {
                            window.location.reload();
                    
                        }
                    });
                }else{

					swal('Error', 'Error no se ha podido Editar el horario!', 'error');

				};;
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("editHorario=true&idHorario="+idHorario+"&hora_inicio="+ hora_inicio +"&hora_finalizar="+hora_finalizar +"&linkZoom="+ linkZoom + "&profesor_id="+ profesor_id+ "&curso_id="+ curso_id + "&nro_client="+ nro_client+ "&diasemana="+diasemana);
    

    })

    $("#registerClientCursoFtF").on('click',  function(event) {
        event.preventDefault();
    
        var id_horario = $("#id_horario").val();
        var mailCliente = $("#mailCliente").val();
        var url = $("#url").val();

        if (mailCliente =='') {
            swal('Error', 'Error correo vacio!', 'error');
            return false;
        }else {
            var mail = EsEmail(mailCliente);
            if(mail == false){
                swal('Error', 'Error correo no correcto!', 'error');
                return false;
            }
        };
    
        var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/horario_ftf_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    swal({
                        title: "¡OK!",
                        text: "Cliente registrado a curso exitosamente!",
                        type: "success",
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    },
                    
                    function(isConfirm) {
                        if (isConfirm) {
                            window.location.reload();
                    
                        }
                    });
                }else if (response == "exist") {

					swal('Error', 'Error Cliente ya esta registrado a este curso!', 'error');

				}else if (response == "no_exist_user") {

					swal('Error', 'Error Cliente no existe!', 'error');

				};
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("addClientCursoftf=true&id_horario="+ id_horario + "&correo_client="+ mailCliente);
    

    })

    $(".statusCursoFtfCliente").on('click',  function(event) {
        var id = $(this).val();
        var url = $("#url").val();

       var ajax = new XMLHttpRequest();

        var URL = url +'src/ajax/horario_ftf_ajax.php';
        var method = "POST";

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var response = ajax.responseText;
                if (response == "ok") {
                    var st = $("#cursoFtfStatus_"+id).attr("class");
                    if(st == "badge badge-danger"){
                      
                        $("#cursoFtfStatus_"+id).attr("class","badge badge-success");
                        $("#cursoFtfStatus_"+id).text("Activo");
                        
                     
                        $("#statusCursoFtf_"+id).addClass("btn-danger");
                        $("#statusCursoFtf_"+id).removeClass("btn-success");
                        $("#statusCursoFtf_"+id).text("Inhabilitar ");
                 

                    }else if(st == "badge badge-success"){

                        $("#cursoFtfStatus_"+id).attr("class","badge badge-danger");
                        $("#cursoFtfStatus_"+id).text("Inactivo");
                 

                        $("#statusCursoFtf_"+id).addClass("btn-success");
                        $("#statusCursoFtf_"+id).removeClass("btn-danger");
                        $("#statusCursoFtf_"+id).text("Habilitar");
                        

                    }
                }else if(response == "error"){
                    console.log("erro");
                }
            }
        }
        ajax.open(method, URL, true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("statusCursoFtfCliente=true&status="+ id);

    })


    function EsEmail(w_email) {
    
        var test = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var emailReg = new RegExp(test);
    
        return responseTestEmail = emailReg.test(w_email);
    }
        
        
});
