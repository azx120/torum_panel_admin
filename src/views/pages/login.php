<head>
<link rel="stylesheet" href="./src/public/css/style_login.css">
</head>
<div class="login">
    <h1>Login</h1>
    <form method="post" id="form-login">
        <input type="text" name="usernameLogin" id="usernameLogin" placeholder="Username" required="required" />
        <input type="password" name="password" id="password" placeholder="Password" required="required" />
        <button type="submit" class="btn btn-primary btn-block btn-large" id="login" name="login">Let me in.</button>
    </form>
</div>
   